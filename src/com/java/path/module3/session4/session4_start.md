# Working with JSON

In this session we are going to cover JSON in Java. The sample project for this module
has examples for the information below. They will be in their own package name.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session4_assignment.md).

---

### Introduction
JSON (Javascript Object Notation) is a text language that is light-weight and independent, which makes
it easy to read. It can be structured as objects or arrays. In this case as an object, it becomes an
unordered collection of name/value pairs. An array variation is an ordered sequence of values.

Here is an example using both structures in a single JSON file.
```
{
    "id": 234,
    "firstname": "John",
    "lastname": "Doe",
    "phone": [
        {
            "type": "cell",
            "number": "555-555-5555"
        },
        {
            "type": "work",
            "number": "111-111-1111"
        }
    ]
}
```

As you can see, using curly braces, we represent an object, while using square brackets, we represent
an array. JSON structure can be made up of strings, numbers, boolean, null along with object and array
structures.

### Working with JSON
Java does not have a native JSON parser, so we have to rely on libraries. There are 4 common libraries
for working with JSON files.
- JSON.simple
- GSON
- Jackson
- JSONP

Jackson is currently the leading used between the four according to Github statistics. JSONP was created
by Oracle, which the java language is a part of, but is separate from the java language. GSON is an
open source library created by Google. The last one: JSON.simple was created by Yidong Fang and is 
meant to be lightweight and simple. In this session we will look at JSON.simple. In session
5 we will explore Jackson as we can use it for JSON and XML.

##### Setup JSON.simple
With this module, we are now using a Maven project. This makes installing third party libraries
easier so you don't have to install them manually. In order to begin, we will need to edit the pom.xml
file located in the root of the application. Before adding any library we need to first provide the
proper outer tag to represent them. This tag is called *dependencies*. Below is what you need to add
to your pom.xml file. Keep in mind that you have to reload the pom.xml file before they take full affect.
```
<dependencies>
    <!-- https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple -->
    <dependency>
       <groupId>com.googlecode.json-simple</groupId>
       <artifactId>json-simple</artifactId>
       <version>1.1.1</version>
    </dependency>    
</dependencies>
```
You will see that each has a groupId, artifactId and version. These will be explained in the module
on Maven. For now, copy the xml code above and paste it in your pom.xml file. Ensure that you are
inside the project xml tag. Each dependency also includes a comment on where it comes from. That is
optional. I included it here so you can see the central location of where they came from.

##### JSON.simple - [documentation](https://code.google.com/archive/p/json-simple/)
This library, or toolkit as they refer to, is designed to be a simple effient way to encode or decode
JSON. It is in full compliance with the JSON spec [RFC4627](https://www.ietf.org/rfc/rfc4627.txt). We
will be working through the various features of this toolkit, so refer to their documentation for additional
information.

**Encoding JSON**
For encoding JSON we can use both JSONObject and or JSONArray. Both extend different collections. We
will be creating json. To write it to file, we would use FileWriter, and utilize the *.json* extension on the file name.

| Name | Type | Implementation |
|----|----|----|
| JSONObject | Object extends HashMap | Implements JSONAware & JSONStreamAware |
| JSONArray | Object extends ArrayList | Implements JSONAware & JSONStreamAware |
| JSONAware | Interface | Single Function Interface - toJSONString() |
| JSONStreamAware | Interface | Single Function Interface - writeJSONString(Writer var1) |

**JSONObject Example**
```
void objectExample() {
    JSONObject obj = new JSONObject();
    
    obj.put("firstname", "Tom");
    obj.put("lastname", "Jones");
    obj.put("email", "someone@email.com");
    
    System.out.println(obj);
}

Output:
{"firstname":"Tom", "lastname":"Jones", "email":"someone@email.com"}
```

**JSONArray Example**
```
String arrayExample(ArrayList<State> states) {
    JSONArray jsonArray = new JSONArray();
    for(State state : states) {
        JSONObject data = new JSONObject();
        data.put("state", state.getState());
        data.put("abbreviation", state.getAbbreviation());
        
        jsonArray.add(data);
    }
    return jsonArray.toJSONString();
}

Output:
[{"state":"Kansas", "abbreviation":"KS"}, {"state":"Missouri", "abbreviation": "MO"}]
```

You can also encode using a stream such as StringWriter which will use the `writeJSONString()`.


##### Decoding JSON
When decoding JSON, you will also use JSONObject and JSONArray, but in conjunction with JSONParser.
You can either read a string of JSON data, or utilize the FileReader to read in an external file. Depending
on how the file is written, will depend on how it is parsed. For JSONArray objects you will use a loop. 
For JSONObjects you can use the get method. Each version, you will have to do a cast from one to 
another depending on how the file was constructed. You can even utilize a view model in your decoding process.

If you want to create JSON using a generator, utilize the [JSON Generator](https://app.json-generator.com/) website.
This is perfect for test data.

See the [Demo Code Project](https://gitlab.com/roger74/java-file-io) for an example.