# Assignments

For this session you will create a file called module3_4.java in the following package:
*com.java.module3*. Utilize the demo project linked in module3_info for reference. Create any additional
files as needed to complete your tasks. When building models for your data, include the toString,
hashcode and equals methods.

### Task 1
Create a json file based on the States xml you built in session 3 and add it to your resources folder.
Write a method that will read the file and only display the state information based on the state bird
of an American Robin. Print out your results to the console to verify that you have the correct number of states.
There should be 3 states.


### Task 2
Add another filter to task 1 to include the Bluebird. Write a method that will take the new results
and write it to a json file called session4-bird.json. Verify that you have the correct information.