# Assignments

Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module3_2.java in the following package: *com.java.module3*. Utilize the demo
project linked in module3_info for reference.

### Task 1
You will utilize the FileInputStream and FileOutputStream for this task. Write a method that will
create an output file called sample-prime.txt. In that file you will need to generate prime numbers
starting from 0 to 200. Each number should be separated by a comma and a space. Write a second method
that will read that file in and print out the results in a single line.

```
/* input example */
2, 4, ...
```

```
/* output example */
2,
4,

```
   

### Task 2
You will utilize the ByteArrayInputStream for this task. Create a file called session2-task2.txt and 
copy the contents from session1-task1.txt into this new file. Write a method that will read the 
file. Print out the results from reading the file in Title case.