# Streams in Detail

In this session we are going to take a deeper look into stream types. We will look into the following
streams:
- FileInputStream & FileOutputStream
- ByteArrayInputStream & ByteArrayOutputStream

The sample project for this module has examples of the below streams.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session2_assignment.md).

---

### FileInputStream - [documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/FileInputStream.html)

This stream is used for reading bytes from a file. To use this class you will need create a **new** object 
instance and utilize one of the below constructors. Another use for FileInputStream is reading raw
bytes like image data.

There are 3 Constructors for FileInputStream:

| Constructor | Description |
|----|----|
| FileInputStream(File file) | Opens a connection to a file using a File object |
| FileInputStream(FileDescriptor fdObj) | Opens a connection to a file using a FileDescriptor object |
| FileInputStream(String name) | Opens a connection to a file with a string that includes its path |

More information for FileDescriptor can be found [here](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/FileDescriptor.html) as we won't be
focussing on that constructor for the below examples.

```
/* The string path constructor */
FileInputStream fis = new FileInputStream("c:/example/sample.txt");

/* The File object example */
File file = new File("c:/example/sample.txt");
FileInputStream fis2 = new FileInputStream(file);
```

In the demo code *ByteStreamExample*, I utilized the File object constructor, but used a set of
methods to access the resource folder in the project. That file constructor is different from the above 
example, but the result was the same.

**Methods of FileInputStream** 
Below are some methods that can be used with a FileInputStream instance. Each could throw an IOException

| Method | Return Type | Description |
|----|----|----|
| close() | void | Closes the input stream and releases any resources associated with that stream. |
| read() | int | Reads a byte of data from the input stream |
| read(byte[] b) | int | Reads a b.length bytes of data from the input stream into an array of bytes |
| read(byte[] b, int off, int len) | int | Reads up to len bytes of data from the input stream into an array of bytes |
| skip(long n) | long | Skips over and discards n bytes of data from the input stream |

### FileOutputStream - [documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/FileOutputStream.html)
A stream used for writing bytes of data to a File or to a FileDescriptor. Some platforms allow a file
to be opened for writing for only one FileOutputStream at a time, so some constructors may fail if the 
file is already open. Another use for FileOutputStream is for writing raw bytes like image data.

There are five constructors for FileOutputStream:

| Constructor | Description |
|------|----|
| FileOutputStream(File file) | Creates a file output stream to write to the file specified by a File object. |
| FileOutputStream(FileDescriptor fdObj) | Creates a file output stream to write to the file descriptor, which represents and existing connection to a file on the file system. |
| FileOutputStream(String name) | Creates a file output stream to write to the file with a specified string path. |
| FileOutputStream(File file, boolean append) | Creates a file output stream to write to the file specified by a File object. |
| FileOutputStream(String name, boolean append) | Creates a file output stream to write to the file with a specified string path. |

```
/* The string path constructor */
FileOutputStream fis = new FileOutputStream("c:/example/sample.txt");

/* The File object example */
File file = new File("c:/example/sample.txt");
FileOutputStream fis2 = new FileOutputStream(file);
```

**Methods of FileOutputStream** 
Below are some methods that can be used with a FileOutputStream instance. Each could throw an IOException

| Method | Return Type | Description |
|----|----|----|
| close() | void | Closes this stream and releases any system resources associated with this stream |
| write(int b) | void | Writes specified bytes to this stream |
| write(byte[] b) | void | Writes b.length bytes from the byte array to this stream |
| write(byte[] b, int off, int len) | void | Writes len bytes from a byte array starting at the offset off to this stream. |
| getFD() | FileDescriptor | Returns the file descriptor with this stream |

### ByteArrayInputStream - [documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/ByteArrayInputStream.html)
A stream containing an internal buffer of bytes that may be read from a stream. It makes use of an
internal counter that keeps track of the bytes supplied by the read method. This stream after its 
close method is executed has no effect and can allow its methods to be called without generating an 
IOException. Read the documentation link above for method information.

There are 2 constructors for ByteArrayInputStream:

| Constructor | Description |
|------|----|
| ByteArrayInputStream(byte[] buf) | Creates a stream using *buf* as its buffer array |
| ByteArrayInputStream(byte[] buf, int offset, int length) | Creates a stream using *buf* as its buffer array | 

- *buf - the input buffer* 
- *offset - offset of the first byte in the buffer*
- *length - max amount of bytes to read from the buffer*

```
/* Byte array constructor */
String sample = "Java File Input";
byte[] sampleArray = sample.getBytes();

ByteArrayInputStream bis = new ByteArrayInputStream(sampleArray);

```

### ByteArrayOutputStream - [documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/ByteArrayOutputStream.html)
A stream in which data is written into a byte array. The buffer grows automatically as data is 
written to it. Data can be retrieved with the toByteArray() and toString(). Calling the close method
on this stream has no effect. The methods can be executed without generating an IOException as a result.
Consult the documentation for information on the methods of this class 

This class has 2 constructors:

| Constructor | Description |
|------|----|
| ByteArrayOutputStream() | Creates a ByteArrayOutputStream stream |
| ByteArrayOutputStream(int size) | Creates a ByteArrayOutputStream stream with a buffer capacity of a specified size in bytes | 

```
/* Buffer size constructor*/
ByteArrayOutputStream baos = new ByteArrayOutputStream(16);
```

