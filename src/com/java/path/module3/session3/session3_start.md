# Working with XML

In this session we are going to cover XML in Java. The sample project for this module 
has examples for the information below. They will be in their own package name.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session3_assignment.md).

---

### Introduction
XML (Extensible Markup Language) is designed format of plain text. It is a markup language similar
to HTML, but not predefined like HTML. You define your own tags and those tags should describe what
data is being saved. It is also W3C Recommended for data storage and data transfer.

```
/* Example XML */
?xml version = "1.0"?>
<Person id=100>
   <FirstName>First</FirstName>
   <LastName>Last</LastName>
   <Address country="US">
      <Street>123 Main St</Street>
      <City>Somewhere</City>
      <State>KS</State>
   </Address>       
   <Email>someone@email.com</Email>
</Person>
```

XML is technology independent, so it can be used by any technology for data or data transfer. XML 
can also be validated using XSD and DTD. XML can however contain a lot of repetitive terms, along with
being large size which may increase the transmission rate and storage costs.

### XML Parsers - [Documentation](https://docs.oracle.com/javase/8/docs/api/index.html?javax/xml/parsers/package-summary.html)
An XML Parser provides a way to access or modify data within an XML document. Java provides different
parsers for XML.
- DOM Parser
- SAX Parser


### DOM Parser
The DOM (Document Object Model) defines an interface that allows programs to access and update XML
documents. XML parsers that support DOM will implement this interface. The DOM the official
recommendation of the W3C.

The DOM is used for both reading HTML and XML documents in a tree structure. One design goal is
for Java code for one DOM Parser run on any DOM compliant parcer without having do do any modifications.
In the above XML example the following elements of the XML is identified below.

| Element | DOM Terms |
|----|----|
| < Person > | root element |
| < ? > elements inside the root  | element nodes |
| First, Last ect.. | Information wrapped around element nodes |
| id="", country="" | Attribute of the element node |

##### DOM Interfaces & Methods
Below are tables for methods and interfaces for the DOM Parser. These are the more common ones, so 
for more information, see the documentation.
[org.w3c.dom](https://download.java.net/java/early_access/loom/docs/api/java.xml/org/w3c/dom/package-summary.html)


| Interface | Description |
|----|----|
| Node | The base datatype |
| Element | The objects of the document |
| Attr | The attribute of the element |
| Text | The content of an element and attr |
| Document | The entire xml document; also referred to as the DOM tree |


| Method | Description |
|----|----|
| Document.getDocumentElement() | Returns the root element |
| Node.getFirstChild() | Returns the first child of a given node |
| Node.getLastChild() | Returns the last child of a given node |
| Node.getNextSibling() | Returns the next sibling of a given node |
| Node.getPreviousSibling() | Returns the previous sibling of a given node |
| Node.getAttribute(attributeName) | Returns the attribute with its requested name of a given node |

There are times when you may need to update and existing XML document. Whether you need to add data to
a document, or update existing values. This is where you would utilize the common methods
listed above. For an example on how the DOM parser is used, updating existing data, and saving a
xml document, see the sample project:
- package com.java.xml
- DomParserExample class
- PersonModel class
- XMLSample class w/main


### SAX Parser
The SAX (Simple API for XML) is an event-based parser for XML documents. This type of parser does
not create a parse tree. Instead, it is a streaming interface for XML. Using a SAX parser will 
receive event notifications about the XML being processed. This happens as it reads the document from
top to bottom. The Java application using a SAX parser must register an event handler with the parser.
Each element in an XML is treated as a token. As each token is identified, callback methods are called
with the available information. The SAX parser is not designed for creating xml files, so consider
using an alternate means for creating xml from code.


This style parser is best used when you want to process the xml document in a linear fashion starting
from the top. XML documents that are not deeply nested and are of large size, which would be a memory
issue for a DOM tree. Data is also available as the parser has consumed the document which makes it
work well with streams.

The downsides are that you will have to manage what data has been seen or if you need to if you need
to change the order of the items. Since this parser is a linear style, random access is not 
achievable unless the data is transferred to a different media.


For detail information about SAX, including its interfaces and methods, read the following
[documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.xml/org/xml/sax/package-summary.html) link.
For sample code refer to the below class files in the demo application.
- com.java.xml package
- SaxExample class
- SaxHandler class
- SaxModifyHandler class
- XMLSample class w/main
