# Assignments

For this session you will create a file called module3_3.java in the following package: 
*com.java.module3*. Utilize the demo project linked in module3_info for reference. Create any additional
files as needed to complete your tasks. When building models for your data, include the toString,
hashcode and equals methods.

### Task 1
Create and xml file containing the information about the US states: Full name, abbreviation, year
it became a state, & the name of its state bird. The outer tag for this xml should be called session3.
Using the DOM parser, read the file and save it back to a new file. Update the new file to include an
id attribute for the state element starting with a value of 1.

### Task 2
Reading the same file created in Task 1, utilize the SAX parser to read and write to a new file. Perform
the same update from task one.