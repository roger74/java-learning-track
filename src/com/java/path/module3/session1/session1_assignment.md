# Assignments

For Module 3 you will create a Maven Project instead of a standard Java Project. There shouldn't be 
any Maven specific things done in Module 3, but if there are, it will be explained in that session.
Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module3_1.java in the following package: *com.java.module3*. Utilize the demo
project linked in module3_info for reference. 

### Task 1
Create a txt file with 5 paragraphs of generated lorem from this [website](https://www.lipsum.com/). Add that txt file to
your resource folder. Create a byte stream example with an output to a file named session1-task1.txt


### Task 2
Re-create your example for Module 1 Session 4 Task 1. Instead of the Scanner, utilize the Standard
Stream input, output, and error. To end the program, you must ask the user if they want to check
any additional years.
