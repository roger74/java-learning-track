# Streams Introduction

In this session we are going to cover the basic understanding of Streams. There will be some writing
and reading of basic txt files in this session. Included with this session will be a working example
of streams. The link for that project will be below.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session1_assignment.md).

---

### Introduction
A **stream** in Java is defined as a sequence of data. This sequence of data can be both read and
written to a file. The **InputStream** is used to read data from the source. The **OutputStream** is used to
write data from to the source. 

The diagram below shows the current streams in Java.

![Stream Diagram 1-1](stream1-1.png)

##### Byte Streams
This type of stream reads and writes in 8-bit bytes. The most common classes used for this is
*FileInputStream* & *FileOutputStream*. We will cover their specifics in the next session, but for this
example, we will keep it simple.

In this example we have a sample.txt file with a poem in it.

```
class ByteStreamExample {
  void copyFile() throws IOException {
    FileInputStream in = null;
    FileOutputStream out = null;
    try {
      in = new FileInputStream("sample.txt");
      out = new FileOutputStream("sample-out.txt");         
      int c;
      while ((c = in.read()) != -1) {
        out.write(c);
      }
    } finally {
      if (in != null) {
        in.close();
      }
      if (out != null) {
        out.close();
      }
    }
  }
}
```

##### Character Streams
This type of stream reads and writes in 16-bit bytes. The most frequently used streams for this type
is *FileReader* & *FileWriter*. Internally FileReader uses FileInputStream and FileWriter uses 
FileInputStream, their main differences is that these streams read/write 2 bytes at a time. Below
is the same example using these streams.

```
class CharacterStreamExample {
  void copyFile() throws IOException {
    FileReader myIn = null;
        FileWriter myOut = null;

        try {
            File myPoem = new File(getClass().getClassLoader()
                    .getResource("sample.txt").getFile());
            myIn = new FileReader(myPoem);
            myOut = new FileWriter("sample-character.txt");
            int c;
            while ((c = myIn.read()) != -1) {
                myOut.write(c);
            }
        } catch (NullPointerException ne) {
            System.out.println(ne.getMessage());
        } catch (IOException fe) {
            fe.printStackTrace();
        } finally {
            if (myIn != null) {
                myIn.close();
            }
            if (myOut != null) {
                myOut.close();
            }
        }
  }
}
```

##### Standard Streams
Java, like other languages, provide a standard I/O where the program can take input from a keyboard
and provide output to a screen. We did this in fundamentals, we utilized the Scanner class to get 
input from the user. Another way to do this is the **InputStreamReader**. 

There are 3 standard streams in Java.

| Stream | Description |
|----|----|
| Standard Input | Used to feed data to the user program with a keyboard as an input device. Represented as a System.in.  |
| Standard Output | Used to output data of the user program with a screen as an output device. Represented as a System.out. |
| Standard Error | Used to output errors of the user program to a screen output device. Represented as a System.err.  |


```
class StandardStreamExample {
    void example() throws IOException {
        InputStreamReader inRead = null;
        try {
          inRead = new InputStreamReader(System.in);
          System.out.println("Enter characters, 'q' to quit.");
          char c;
          do {
            c = (char) inRead.read();
            System.out.print(c);
          } while(c != 'q');
        }finally {
          if (inRead != null) {
            inRead.close();
          }
        }
    }
}
```

