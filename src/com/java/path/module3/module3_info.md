# File I/O

Up to this point we have worked with data that we create on our own. In this module we will look at 
how to read and write files. We will work with plain text, XML & JSON style files. There are libraries
that can make this easier to work with. We will explore those, but not until we have gone through the
conventional way. These techniques can also be used when communicating outside our program. That aspect
will be looked at further in the next module. 

This module will utilize a Maven Project for its demo and reference instead of a basic Java Project. 
If any Maven related information is required, it will be outlined in the session specifically. Going
forward, all projects in this learning Track will use Maven Projects.

We will explore the following sessions below:

### [Streams Introduction](session1/session1_start.md)
### [Streams in Detail](session2/session2_start.md)
### [Working with XML](session3/session3_start.md)
### [Working with JSON](session4/session4_start.md)
### [Working with Jackson](session5/session5_start.md)

---
The below project will have demos for each session in this module.

#### [Demo Project](https://gitlab.com/roger74/java-file-io)
