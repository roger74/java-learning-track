# Working with Jackson

In this session we are going to cover the Jackson Library for Java. The sample project for this 
module has examples for the information below. They will be in their own package name.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session5_assignment.md).

---

### Introduction - [Documentation](https://github.com/FasterXML/jackson)
What is Jackson? This library is best known for parsing JSON files and is well known in the industry.
It also has the ability to process other data formats with XML being one of them. While we won't be
covering every data format this library can work with, we will look at JSON and XML specifically.

The recommended way to use Jackson is with Maven. Like Session 3, we will add reference to this library
in the pom.xml file in our projects. The most current version as of the time of this documentation
will be presented below for your reference. You will see three listed. That is because the core
was broken into 3 manageable modules. The databind module below depends on the other two, so you can
just have it as your dependency, and it will add the other two.

```
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-core</artifactId>
    <version>2.13.1</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-annotations</artifactId>
    <version>2.13.1</version>
</dependency>
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.13.1</version>
</dependency>
```

##### Simple Read/Write Operations with Object Mapper
There are two methods we can utilize for simple operations using Object Mapper. For reading json, whether
it is from a file, or a String of json we can use the readValue method. For writing json from an object
we can utilize the writeValue method. For writing json as a string, we can use the writeValueAsString
or writeValueAsBytes methods.

```
// Sample class object
public class House {
    private final int windowCount;
    private final int doorCount;
    private final String interiorPaintColor;   
    
    /* getter and constructors below */ 
}
```

```
// Read value sample
void readSample1() {
    ObjectMapper objectMapper = new ObjectMapper();
    String houseJson = "{ \"windowCount\" : 22, \"doorCount\" : 10, \"interiorPaintColor\" : \"medium Gray\" }";
    House house = objectMapper.readValue(houseJson, House.class);    
}

// File from resource
void readSample2() {
    ObjectMapper objectMapper = new ObjectMapper();
    House house = objectMapper.readValue(new File(getClass().getClassLoader().getResource("house.json").getFile()), House.class)
}
```

Writing to json with the same example as above
```
void writeSample1() {
    ObjectMapper mapper = new ObjectMapper();
    House house = new House(22,10, "medium gray");
    mapper.writeValue(new File("house-sample.json"), house);
}
// Also write as string
String sample = mapper.writeValueAsString(house);
```

If you need to get a specific object you can utilize JsonNode. This combined with the Object mapper
will allow you to get a value based on the key.
```
void nodeSample() {
    ObjectMapper objectMapper = new ObjectMapper();
    String houseJson = "{ \"windowCount\" : 22, \"doorCount\" : 10, \"interiorPaintColor\" : \"medium Gray\" }";
    JsonNode jNode = objectMapper.readTree(houseJson);
    String count = jNode.get("doorCount").asText();
}
```

See sample project for the above examples

**Working with JSON Array**
We can create a List (ArrayList) from parsing a json array. We can do this using a TypeReference. This
utilizes the object mapper's read value method that can take the data and TypeReference of the list you
want to use. You can also do this with a Map as well, but that is usually done with objects instead of 
arrays.

See the sample project for a List example using a file of generated data called workers.json.

The works.json has an array inside an array. As you look at the WorkerModel class file, you will see
that an inner class was created specifically to address the inner-most array on the json file. As a
result, this inner class must be a static class for the Jackson code to work properly. Also in
the WorkerModel class file are Annotations. These help map the json file data to the proper variable
in the class file. Jackson has a number of different Annotations. For an example of how those work
beyond the current example can be seen [here](https://www.baeldung.com/jackson-annotations).

##### Jackson and XML
For us to serialize and deserialize xml we need to add a new repository. This is so we can work with
xml files using the same library, but different module. Using Jackson for working with XML is not a
true replacement for JAXB. JAXB stands for (Java Architecture for XML Binding) and was part of the 
java jdk in java7. In Java 8-9 it was deprecated as it was being removed from the JDK. As of JDK11,
it is no longer in the JDK and has to be added by maven package. For more information on JAXB, read
the following [blog](https://www.jesperdj.com/2018/09/30/jaxb-on-java-9-10-11-and-beyond/) post.

```
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.dataformat/jackson-dataformat-xml -->
<dependency>
    <groupId>com.fasterxml.jackson.dataformat</groupId>
    <artifactId>jackson-dataformat-xml</artifactId>
    <version>2.13.1</version>
</dependency>
```
We can utilize XmlMapper which is a subclass of ObjectMapper to work specifically with xml files. Using
the writeValue method we can provide a file and the model class to use. For this simple use,  you will
need to have either the model's constructor set to accept values, or have defined values inside the class.
Using the HouseModel from the sample project we can show how this works below.

```
void writeXMLData() throws IOException {
        XmlMapper mapper = new XmlMapper();
        mapper.writeValue(new File("house-xml.xml"),new HouseModel(25,13,"Beige"));
    }
```

For deserializing we can utilize a model and data read from. When using a model you may need to 
include annotations depending on the xml data. This model is based on the students.xml file located
in the resource folder of the sample code. It represents one module of that data. You will see
that included are the annotations including the attribute for id. The classes for this example include:
- StudentModel
- JacksonXMLExample - method (readXMLData)
```
/* Model */
@JacksonXmlRootElement(localName = "student")
public class StudentModel {
    @JacksonXmlProperty(isAttribute = true, localName = "id")
    private String id;

    @JacksonXmlProperty(localName = "firstname")
    private String fName;

    @JacksonXmlProperty(localName = "lastname")
    private String lName;

    @JacksonXmlProperty(localName = "subject")
    private String subject;

    @JacksonXmlProperty(localName = "grade")
    private char grade;
    
    //setters, getters and constructor
```

The output below will show based on the toString() in the model
```
/* Method implimentation */
void readXMLData() throws IOException {
    XmlMapper mapper = new XmlMapper();
    String student = "<student id=\"753951\">\n" +
           "        <firstname>John</firstname>\n" +
           "        <lastname>Smith</lastname>\n" +
           "        <subject>Science</subject>\n" +
           "        <grade>A</grade>\n" +
           "    </student>";
    StudentModel studentList = mapper.readValue(student, StudentModel.class);
    System.out.println(studentList.toString());
}

Output:
StudentModel{id='753951', fName='John', lName='Smith', subject='Science', grade=A}
```

For us to expand on this and handle the students.xml file itself requires a little more effort. Since
the outer tag in the file is class, we begin by building that model, but a student model will need to
be created to represent the list of objects in the file. Jackson annotations are used to correctly map
everything. For an example of how this works, see the sample demo with the follow files.
- ClassModel
- Student
- JacksonXMLSample - method(readXMLFromFile)