# Assignments

For this session you will create a file called module3_5.java in the following package:
*com.java.module3*. Utilize the demo project linked in module3_info for reference. Create any additional
files as needed to complete your tasks. When building models for your data, include the toString,
hashcode and equals methods.

### Task 1
Write a method that will read the states json file you created in session 4 using the Jackson library.
Once you have read in the data, save it to a new file called session5-states.json. Also print the 
abbreviations for each state to the console.

### Task 2
Write a method that will read the xml version of the states. Display the results into the console.