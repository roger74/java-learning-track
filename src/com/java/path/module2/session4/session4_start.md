# Java 15 & 16
In this module we will cover the features added, removed or deprecated from Java 15 & 16.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session4_assignment.md).

---

### Java 15
The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/15/)

- **JEP 339 Edwards-Curve Digital Signature Algorithm (EdDSA)**
    - [Documentation](https://openjdk.java.net/jeps/339)
    - A modern elliptic curve signature scheme that has several advantages over the existing
  schemes in the JDK. The goal of this implementation is for the scheme to be a standardization based
  on RFC 8032
- **JEP 360 Sealed Classes**
    - [Documentation](https://openjdk.java.net/jeps/360)
    - Enhance the Java Language with sealed classes and interfaces, which would restrict which classes
  or interfaces may extend or implement them.
- **JEP 371 Hidden Classes**
    - [Documentation](https://openjdk.java.net/jeps/371)
    - Classes that cannot be used directly and are intended for use by frameworks that generate 
  classes at runtime and are used via Reflection.
- **JEP 372 Remove the Nashorn JavaScript Engine**
    - [Documentation](https://openjdk.java.net/jeps/372)
    - This feature was deprecated in Java 11 and was officially removed in this release.
- **JEP 373 Reimplement the Legacy DatagramSocket API**
    - [Documentation](https://openjdk.java.net/jeps/373)
    - This is a follow-on to JEP 353. This new implementation would replace *java.net.DiagramSocket*
  and *java.net.MulticastSocket* APIs
- **JEP 374 Disable and Deprecate Biased Locking**
    - [Documentation](https://openjdk.java.net/jeps/374)
    - Determining the need to continue support for this legacy synchronization; This feature will be
  disabled and deprecate all related command-line options.
- **JEP 375 Pattern Matching for instanceof (Second Preview)**
    - [Documentation](https://openjdk.java.net/jeps/375)
    - This feature is a re-preview of JDK 14s JEP305. No changes were made sense its 1st preview.
- **JEP 377 ZGC: A Scalable Low-Latency Garbage Collector**
    - [Documentation](https://openjdk.java.net/jeps/377)
    - This feature was in Experimental as of JDK 11 JEP 333 and becomes a product feature in JDK 15.
- **JEP 378 Text Blocks**
    - [Documentation](https://openjdk.java.net/jeps/378)
    - This feature was proposed in JEP 355 JDK 13 as a preview feature. A text block is a multi-line
  string literal, which avoids most escape sequences, auto-formats the string in a predictive way.
- **JEP 379 Shenandoah: A Low-Pause-Time Garbage Collector**
    - [Documentation](https://openjdk.java.net/jeps/379)
    - This features changes from experimental to a product feature
- **JEP 381 Remove the Solaris and SPARC Ports**
    - [Documentation](https://openjdk.java.net/jeps/381)
    - This JEP removes all source code specific to Solaris OS and SPARC architecture. These ports
  were deprecated for removal in JDK 14.
- **JEP 383 Foreign-Memory Access API (Second Incubator)**
    - [Documentation](https://openjdk.java.net/jeps/383)
    - This feature is going through a Second Incubator that was proposed in JDK 14. Changes made for
  this JDK are listed in the documentation. 
- **JEP 384 Records (Second Preview)**
    - [Documentation](https://openjdk.java.net/jeps/384)
    - Originally proposed in JDK 14. This re-preview includes refinements based on feedback.
- **JEP 385 Deprecate RMI Activation for Removal**
    - [Documentation](https://openjdk.java.net/jeps/385)
    - This feature has been optional since Java 8 and is deprecated as part of this JDK. Although this
  RMI Activation will be deprecated, RMI itself will not.


##### Sealed Classes
The goal of this feature is to allow the author of the class or interface to control the code in
which would be responsible for implementation. This would provide a more declarative way than using
and access modifier. This would also help facilitate future implementations of pattern matching.

Using the **sealed modifier** word during class declaration, the class then specifies which classes may extend
it using the **permits clause** after the class, but before the class curly braces. The below 
implementation is done when the subclasses are located in the same module or package name.

```
package com.java.learning

public abstract sealed class SomeClass
    permits SomeNewClass, SomeOldClass, SomeOtherClass {
      // code goes here
    }
```

If the subclasses are in different packages of the same named module, their permits must utilize the
fully qualified class name, which includes the package name.

```
package com.java.learning

public abstract sealed class SomeClass
    permits com.java.module2.SomeNewClass
    permits com.java.module2.session4.SomeOldClass
    permits com.java.starting.SomeOtherClass {
      // code goes here
    }
```
For more information on Sealed Classes, refer to the documentation link above.


### Java 16
The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/16/)

- **JEP 338 Vector API**
  - [Documentation](https://openjdk.java.net/jeps/338)
  - Vector computations are a sequence of operations on vectors. These vectors are a fixed sequence
  of scalar values, where scalar values correspond to the number of hardware defined vector lanes. This
  API is an initial iteration of an incubator module: *jdk.incubator.vector*
- **JEP 347 Enable C++ 14 Language Features**
  - [Documentation](https://openjdk.java.net/jeps/347)
  - This feature allows C++ 14 language features in the JDK C++ source code and provide specific
  guidance about the features used in HotSpot code.
- **JEP 357 Migrate from Mercurial to Git**
  - [Documentation](https://openjdk.java.net/jeps/357)
  - Migrating the OpenJDK Source code from Mercurial to Git.
- **JEP 369 Migrate to GitHub**
  - [Documentation](https://openjdk.java.net/jeps/369)
  - Migrates the new git source code in JEP 357 to GitHub.
- **JEP 376 ZGC: Concurrent Thread-Stack Processing**
  - [Documentation](https://openjdk.java.net/jeps/376)
  - Move ZGC thread-stack from safepoints to a concurrent phase.
- **JEP 380 Unix-Domain Socket Channels**
  - [Documentation](https://openjdk.java.net/jeps/380)
  - The goal of this JEP is to support all the features of Unix domain sockets that are common across
  major Unix platforms and Windows.
- **JEP 386 Alpine Linux Port**
  - [Documentation](https://openjdk.java.net/jeps/386)
  - Port the JDK to Alpine Linux and other Linux distributions that use musl as their primary C library
  on both x64 & AArch64 architectures
- **JEP 387 Elastic Metaspace**
  - [Documentation](https://openjdk.java.net/jeps/387)
  - Return unused HotSpot class metadata memory to the OS for more promptly. This reduces the metaspace
  footprint and simplifies the metaspace code to reduce maintenance costs.
- **JEP 388 Windows/AArch64 Port**
  - [Documentation](https://openjdk.java.net/jeps/388)
  - Port the JDK to Windows/AArch64.
- **JEP 389 Foreign Linker API (Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/389)
  - An API introduction that offers statically typed, pure java access to native code.
- **JEP 390 Warnings for Value-Based Classes**
  - [Documentation](https://openjdk.java.net/jeps/390)
  - Designate primitive wrapper classes as value based and deprecate their constructors for removal,
  prompting new deprecation warnings.
- **JEP 392 Packaging Tool**
  - [Documentation](https://openjdk.java.net/jeps/392)
  - Provide the jpackage tool for packaging self-contained java applications. This tool was introduced
  in JDK 14 and remains as an incubating tool in JDK 15. Now it is a production ready feature.
- **JEP 393 Foreign-Memory Access API (Third Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/393)
  - Based on feedback this feature now in its 3rd incubation introduces and API to allow java programs
  to safely access foreign memory outside the java heap.
- **JEP 394 Pattern Matching for instanceof**
  - [Documentation](https://openjdk.java.net/jeps/394)
  - This JEP finalizes the feature that was introduced in JDK 14, and re-proposed in JDK 15.
- **JEP 395 Records**
  - [Documentation](https://openjdk.java.net/jeps/395)
  - This JEP finalizes this feature that was introduced in JDK 14.
- **JEP 396 Strongly Encapsulate JDK Internals by Default**
  - [Documentation](https://openjdk.java.net/jeps/396)
  - Strongly encapsulate all internal elements of the JDK by default. Exceptions are critical internal
  APIs like *sun.misc.Unsafe*. 
- **JEP 397 Sealed Classes (Second Preview)**
  - [Documentation](https://openjdk.java.net/jeps/397)
  - This second preview is based on the original proposal from JDK 15. This new preview introduced 
  refinements to the original preview. **sealed**, **non-sealed** and **permits** are introduced as
  contextual keywords


##### Pattern Matching
With this finalization the following refinements were made
- The restriction for pattern variables that are implicitly final was lifted. This restriction lift
was to reduce asymmetries between local and pattern variables.
- A compile-time error will execute for a pattern instanceof expression that compares an expression 
of a subtype against a super type. The expression always succeeds and is considered pointless, where 
as the opposite fails and produces a compile-time error.

```
// Example old implementation of instanceof
void something() {
  if(obj instanceof String) {
    String value = (String) obj;
    // other code here
  }
}

// Pattern matching implementation
void other() {
  if(obj instanceof String value) {
    // other code here
  }
}

// The if statement String value is the type pattern
// You no longer have to cast types like the first example
```
One thing to consider using this inline pattern matching is the concept of poisoned state. This happens
when the condition becomes false. The pattern matching variable is still considered in scope and as
a result can no longer be declared again, withing the method that this condition was written.

For more information, please read the documentation defined in the JEP.

### Records
*Record* classes are a new kind of class that helps model plain data aggregates with less code than
normal classes. One of the key differences between a record and a class the ability to decouple a 
class's API from its internal representation but a record class's declaration becomes more concise 
as a result.

The automatic members that come with each class are automatically created in records. These types of 
members should be defined in normal classes. They consist of:
- equals & hashcode methods
- toString method

With records being a transparent carrier of their data, the following information is part of its makeup.
- For each component in its header, a private final attribute and public accessor method are generated.
- A canonical constructor is created with the same signature as the record declaration. These values 
are automatically assigned to the private final variables.
- equals & hashcode methods are auto generated and configured based on the variable signature declared
in the record that ensures the values are equal and are of the same type.
- toString method is aut generated to return a string representation of all the record components 
along with their names.

``` 
// record implemintation
record Something(int alpha, String beta) {  }
```

``` 
// class implemintation
class Something {
  private final int alpha;
  private final String beta;
  
  Something(int alpha, String beta) {
    this.alpha = alpha;
    this.beta = beta;
  }
  
  int getAlpha() { return alpha; }
  String getBeta() { return beta: }
  
  public boolean equals(Object obj) {
    if(!(obj instanceOf Something)) return false;
    Something other = (Something)obj;
    return other.alpha = alpha && other.beta = beta; 
  }
  
  public int hashCode() {
    return Objects.hash(alpha, beta);
  }
  
  public String toString() {
    return String.format("Something[alpha=%d, beta=%d]", alpha, beta);
  }
}
```

**Records & Constructors**
Constructors in normal classes are automatically given a default constructor if no constructor is
defined. With records however, a constructor that is created automatically is a canonical constructor
which assigns all the private fields as they are defined in the record's declaration. You can declare
a record constructor explicitly.

An **explicitly** created record constructor is defined like its automatic version. Utilizing its 
parameters based on the private fields of the defined record. You can also declare this constructor
in a more compact way as well. This allows you to add any code needed for the record as part of the 
constructor. The compact way negates the need to assign parameters to the private fields.

```
// conventional constructor defined in a record
record Something(int alpha, String beta) {
  Something(int alpha, String beta) {
    System.out.println(String.format("I have (%d) for (%d) days", beta, alpha);
    this.alpha = alpha;
    this.beta = beta;
  }  
} 

// compact constructor version in a record
record Something(int alpha, String beta) {
  Something {
    System.out.println(String.format("I have (%d) for (%d) days", beta, alpha);
  }
}
```

There is a lot more information on Records than what is defined here.