# Assignments

Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module2_4.java. Create any additional files as needed for the task. Include a
main method to test your code.

### Task 1
Read through the documentation for JEP 395 Records. Be sure to take notes. Create an example of a 
record that has a minimum of 4 parameters. It should be created in its own file instead of the file 
in the directions.

In the file module2_4.java, create an example of a local record class. This should include comments
on how it is being utilized.

Define the rules for record classes in the file that the record is defined in. These rules should 
be written in your own words.

### Task 2 
Read through JEP 394: Pattern Matching for instanceof. Write an example using pattern matching in 
instanceof. Ensure that the condition is correct so there won't be a poisoned state.

### Task 3
Read through JEP 397 Sealed Classes documentation. Write an example of its implementation.