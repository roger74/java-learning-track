# Java 11 and the upgrade path
In this module we are going to cover what makes up Java 11 and suggestions and process for upgrading
from Java 8 to Java 11.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session2_assignment.md).

---

### Java 11
This java release came out in September 2018 and became the next LTS release after Java 8. Beginning
with this release the LTS support became no longer free. Oracle does provide an OpenJDK release that
can be used without charge. 

The below developer changes are included with this release

- **JEP330** Running Java files from Command Line or Terminal
  - [Documentation](https://openjdk.java.net/jeps/330)
- **JEP321** HTTP Client now is standard
  - We will cover this in detail in another module
  - [Documentation](https://openjdk.java.net/jeps/321)
- **JEP323** Local-Variable Syntax for Lambda
  - [Documentation](https://openjdk.java.net/jeps/323)
- **JEP181** Nest Based Access Control
  - [Documentation](https://openjdk.java.net/jeps/181)
- Collection to Array
- New File Methods
  - We will cover this in detail in another module
- New String Methods

##### Running Java Files
Before Java 11 you have to execute 2 terminal commands in order to run a java source file.

```
javac SomeClass.java
java SomeClass

Hello Java!
```

Now you can do this with only calling the *java* command. Keep in mind that this only works for 
single file sources. If you want to compile a program with more than one java file, you will still
need to do it the original way.

```
java SomeClass.java

Hello Java!
```

##### Local Variable Syntax for Lambda
This enhancement allows the use of a local-variable syntax when declaring formal parameters of
implicit type lambda expressions. The syntax is in the form of the **var** keyword.

```
// Implicitly typed lambda expressions
(x, y) -> x.process(y)
```

In Java 10, an Implicit type inference was created using the **var** keyword. This keyword is used
locally as a placeholder for the datatype, so it could represent any datatype. This keyword only 
works in context with local declaration. It does not work at the class level.

```
void someMethod() {
  // int
  var alpha = 100;
  System.out.println(alpha * 2);
}
```

With lambda expressions we can now utilize the same **var** keyword.

```
(var x, var y) -> x.process(y);
```

Here is a working example
``` 
// The annotation in this example will allow the compiler to determine a null value.
void someMethod() {
  var myArray = new Integer[]{5, 10, 15, 20, 25};
  long value = Arrays.stream(myArray).filter(
  (@Nonnull var alpha) -> (alpha != null && alpha > 5)).count();
  System.out.print(value);
}
```

##### Nest Based Access Control
The concept of *nests* is introduced to existing nested types. This allows classes that are part of
the same code entity, but compiled as their own class files given access to other private members
without the need of accessibility-broadening bridge methods. Attempting to do this using Reflection
techniques will throw a IllegalAccessError.

##### Collection to Array
A new default method was added to `java.util.Collection` called `toArray(IntFunction)`. This method
is an overload of `toArray(T[])` that takes an array as an argument. This method will allow 
collection elements to be created in a new array of the desired type. 

The **IntFunction** is a functional interface. [Documentation](https://docs.oracle.com/javase/8/docs/api/java/util/function/IntFunction.html)

```
ArrayList<String> list = new ArrayList<>();
list.add("Learn");
list.add("Java 11");
list.add("content");

// Using the :: operator to refer to the String[] default constructor
String[] myArray = list.toArray(String[]::new);

for(int i = 0; i < myArray.length; i++) {
  System.out.println(myArray[i]);
}
```

##### New String Methods
New methods were added to the String class. Not reported in the JDK 11 release information, but are
included in the documentation on String. [Documentation](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/String.html)

The following methods were added:
- `isBlank()`
  - Returns true if the string is empty or contains empty space codepoints
- `lines()`
  - Returns a stream of lines extracted from this string, separated by line terminators.
- `repeat(int count)`
  - Returns a string whose value is the concatenation of this string repeated *count* times.
- `strip()`
  - Returns a string whose value is this string, with all leading and trailing empty space removed.
- `stripLeading()`
  - Returns a string whose value is this string with all leading empty space removed.
- `stripTrailing()`
  - Returns a string whose value is this string with all trailing empty space removed.

### Performance Enhancements & Experimentation
- JEP 328: Flight Recorder
  - A data collection framework with low-overhead for troubleshooting applications. This feature is
  also now open source in the Open JDK. You can use it to get diagnostic and profiling data from
  a running application.
- JEP 318: Epsilon Experimental No-Op Garbage Collector
  - Create a GC that manages memory allocation but doesn't implement memory reclamation. When the 
  memory heap is exhausted, the JVM shuts down.
- JEP 309: Dynamic Class-File Constants
  - [Documentation](https://openjdk.java.net/jeps/309)
- JEP 315: Improve Aarch64 Intrinsics
  - [Documentation](https://openjdk.java.net/jeps/315)

### Removed & Deprecated 
Some features have been removed or deprecated as of Java 11. The removed features in some cases are
still available via separate library or third-party sites.

- JavaEE & CORBA
  - These modules were deprecated in Java 9 but are available on third-party websites. They have 
  been officially removed as of Java 11.
- JMC & JavaFX
  - These modules are no longer part of Java 11 and are available as standalone versions
- Nashorn Javascript Engine, including JJS Tool
  - This module has been deprecated
- Pack200 Compression scheme for JAR files
  - This module has been deprecated

There were more things removed or deprecated in JDK 11. Refer to this [Documentation](https://www.oracle.com/java/technologies/javase/11-relnote-issues.html)
for more detailed information.

### Upgrading Paths to Java 11
For information on Upgrading to Java11 refer to the [Oracle's Blog](https://blogs.oracle.com/javamagazine/post/its-time-to-move-your-applications-to-java-17-heres-why-and-heres-how) on Java Magazine.
Rather than re-writing this information, that blog post covers the most important items to consider
when making those changes from Java 8 to 11.