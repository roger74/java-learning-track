# Assignments

Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module2_2.java. Include a main method to test your code.

### Task 1
Read all the JEP Documentation links below.
- **JEP330** Running Java files from Command Line or Terminal
    - [Documentation](https://openjdk.java.net/jeps/330)
- **JEP321** HTTP Client now is standard
    - [Documentation](https://openjdk.java.net/jeps/321)
- **JEP323** Local-Variable Syntax for Lambda
    - [Documentation](https://openjdk.java.net/jeps/323)
- **JEP181** Nest Based Access Control
    - [Documentation](https://openjdk.java.net/jeps/181)

### Task 2
Write 2 examples of Local-Variable Syntax for Lambda. Utilize the :: method operator for each.

### Task 3
Utilizing Collection to Array, create examples using the following Collections
- HashSet
- TreeSet
- Stack

### Task 4
Create a method that demonstrates the new String Methods. Print out the original strings used
along with the method versions.