# Java 17
In this module we will cover the features added, removed or deprecated in Java 17.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session5_assignment.md).

---

The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/17/)

The JDK 17 will be a long-term support (LTS) release from most vendors.

- **JEP 306 Restore Always-Strict Floating-Point Semantics**
  - [Documentation](https://openjdk.java.net/jeps/306)
  - Make floating-point operations strict rather than both strict and different default semantics.
- **JEP 356 Enhanced Pseudo-Random Number Generators**
  - [Documentation](https://openjdk.java.net/jeps/356)
  - Provide new interfaces and implementations for pseudo-random number generators. 
- **JEP 382 New macOS Rendering Pipeline**
  - [Documentation](https://openjdk.java.net/jeps/382)
  - Implement a Java 2D internal rendering pipeline for macOS using the Apple Metal API.
- **JEP 391 macOS/AArch64 Port**
  - [Documentation](https://openjdk.java.net/jeps/391)
  - Port the JDK to macOS/AArch64.
- **JEP 398 Deprecate the Applet API for Removal**
  - [Documentation](https://openjdk.java.net/jeps/398)
  - Deprecate the Applet API for removal as all web browser vendors removed Java browser plugin
  support, or plan to do so.
- **JEP 403 Strongly Encapsulate JDK Internals**
  - [Documentation](https://openjdk.java.net/jeps/403)
  - Strongly encapsulate all internal elements of the JDK, except critical internal APIs such as
  *sun.misc.Unsafe*. Single command-line option to relax the strong encapsulation from JDK 9 - 16 is
  no longer possible.
- **JEP 406 Pattern Matching for switch (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/406)
  - Enhance the Java language with pattern matching for switch expressions, and extensions to the 
  language of patterns.
- **JEP 407 Remove RMI Activation**
  - [Documentation](https://openjdk.java.net/jeps/407)
  - Remove the Remote Method Invocation (RMI) activation mechanism, while preserving the rest of RMI.
- **JEP 409 Sealed Classes**
  - [Documentation](https://openjdk.java.net/jeps/409)
  - The finalized implementation of sealed classes and interfaces. They restrict which other classes
  or interfaces may extend or implement them.
- **JEP 410 Remove the Experimental AOT and JIT Compiler**
  - [Documentation](https://openjdk.java.net/jeps/410)
  - Removed these features as they saw little use and the effort left to maintain it is significant.
- **JEP 411 Deprecate the Security Manager for Removal**
  - [Documentation](https://openjdk.java.net/jeps/411)
  - This manager dates from Java 1.0 and is not the primary securing client-side code and is now
  deprecated and slated for removal.
- **JEP 412 Foreign Function & Memory API (Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/412)
  - An API which Java programs can interoperate with code and data outside the Java Runtime.
- **JEP 414 Vector API (Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/414)
  - A second Incubator introducing an API to express vector computations that compile at runtime to 
  optimal vector instructions on supported CPU architectures.
- **JEP 415 Context-Specific Deserialization Filters**
  - [Documentation](https://openjdk.java.net/jeps/415)
  - Allow applications to configure context-specific and dynamically-selected deserialization 
  filters via a JVM-wide filter factory that is invoked to select a filter for each individual 
  deserialization operation.

##### Pattern Matching for Switch (Preview)
This feature is motivated based on the *instanceof* operator pattern matching implemented in JDK 16.
Introduce two new kinds of patterns: *guarded patterns* and *parenthesized patterns*. This implementation
is meant to expand the current use of the switch and not replace the current working versions of them.

Traditional switch statements and expressions would throw NullPointerException if a selector evaluates
to null, so null checking was done outside the switch like the following example:

```
void someMethod(String value) {
  if(value == null) {
    System.out.println("Value Missing");
    return;
  }
  switch(value) {
    case "Happy" -> System.out.println("Happy Java");
    default -> System.out.println("OK");
  }
}
```

With patterns and the allowance for a selector expression of any type, we can now integrate the null
check into the switch expression.

```
void someMethod(String value) {
  switch(value) {
    case null -> System.out.println("Value Missing");
    case "Happy" -> System.out.println("Happy Java");
    default -> System.out.println("OK");
  }
}
```

##### Sealed Classes
The goals of this feature is to allow the developer of a class or interface to control which code
is responsible for implementation. This approach is to provide a more declarative way than using
access modifiers. This will allow support for pattern matching by providing a foundation for pattern
investigation.

The implementation of a sealed class consists of the sealed keyword. Any class that needs permission
to extend will be defined after any extends and implements clauses, and before the curly brace. The
extended classes will use the permits keyword and can be chained by commas or defined by fully
qualified class name if they are in different packages.

```
public abstract sealed class SomeClass
    permits SomeOther, OtherClass {
      // code goes here
    }
```


```
package com.java.example

public abstract sealed class House
    permits com.java.other.Condo,
    permits com.java.other.TwoStory {
      // code goes here
    }
```

