# Assignments

Complete the following code tasks to reinforce the session 5 content. Create at least 1 class file
to represent your session unless otherwise instructed. For this session you will create a file
called module2_5.java. This file will be used for some tasks. You will also create other java files
as defined in their tasks.

### Task 1
Read through JEP 406 Pattern Matching for Switch documentation. Write an example of using guarded patterns and an
example of using parenthesized patterns.

### Task 2
Read through JEP409 Sealed Class documentation. Creating new class files, re-write the abstract class
about comic books from Module 1 Session 3 Task 1. If you did not create an Abstract class for that
task, create one here and utilize the permits keyword and assign them to your comic classes.