# Java 8+
In this module we will look at what has changed from Java 8 to the most current java version. While
some of these changes are behind the hood of the language, there are specific things we can explore
and build examples from. As of the time of this module, Oracle has 6 month release cycles for the
java language. 

Java 8 and Java 11 are considered LTS versions of the Java language. LTS or Long-Term-Support is
what Oracle uses as their official support versions. Java 17 is the newest LTS since Java 11. Each
release comes with Java Enhanced Proposals (JEP) followed by a number identifier. Most companies 
are still using Java 8 and 11 because of their LTS status, but that could change with the Java 17
release which came in September 2021.

We will explore the following sessions below:
#### [Java 8](session1/session1_start.md)
#### [Java 11 and the upgrade path](session2/session2_start.md)
#### [Java 12 through 14](session3/session3_start.md)
#### [Java 15 through 16](session4/session4_start.md)
#### [Java 17](session5/session5_start.md)
