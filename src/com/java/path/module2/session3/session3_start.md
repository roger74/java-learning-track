# Java 12 through 14
In this module we will cover the features added, removed or deprecated from Java 12 through 14. 

Study the information below. Take notes. Once complete, begin your [Session Assignment](session3_assignment.md).

---

### Java 12
The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/12/)

- **JEP 189 Shenandoah: A Low-Pause-Time Garbage Collector (Experimental)**
  - [Documentation](https://openjdk.java.net/jeps/189)
  - A new garbage collection algorithm for reducing pause times in the GC by evacuating work loads
  with running threads. These pause times are separate from heap size
- **JEP 230: Microbenchmark Suite**
  - [Documentation](https://openjdk.java.net/jeps/230)
  - A suite of Micro-benchmarks that make it easy to create and run existing ones
- **JEP 325: Switch Expressions (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/325)
  - Extends the existing Switch statement, so it can be either a statement or expression. Both
  styles can be used as either traditional or simplified scoping.
  - This feature is a preview and is subject to change.
- **JEP 334: JVM Constants API**
  - [Documentation](https://openjdk.java.net/jeps/334)
  - An API that models descriptions of class-file and run-time key artifacts like constants that
  are loadable from the constant pool.
- **JEP 340: One AArch64 Port, Not Two**
  - [Documentation](https://openjdk.java.net/jeps/340)
  - Remove all the sources related to the arm64 port while retaining the 32-bit ARM and 64-bit aarch64 ports.
- **JEP 341: Default CDS Archives**
  - [Documentation](https://openjdk.java.net/jeps/341)
  - Enhancement to the JDK build process to generate CDS archives using the default class list on 64-bit platforms.
- **JEP 344: Abortable Mixed Collections for G1**
  - [Documentation](https://openjdk.java.net/jeps/344)
  - Provide G1 mixed collections the ability to abort if they might exceed the pause target.
- **JEP 346: Promptly Return Unused Committed Memory from G1**
  - [Documentation](https://openjdk.java.net/jeps/346)
  - Enhances the G1 Garbage collector to return the Java heap memory to the OS when idle automatically.

##### Switch Expressions Example
In these examples will include both the statement and expression versions to compare to each other.

```
/* Basic switch statement with its break statements that make it verbose and can often mask
 debug errors and missing breaks can cause accidental fall-through. */
 
String message = "My favorite days are for ";
switch(day) {
  case FRIDAY:
  case SATURDAY:
  case SUNDAY:
    message += "Weekend Fun";
    break;
  case MONDAY:
  case WEDNESDAY:
    message += "Pasta Party";
    break;
  case TUESDAY:
    message += "Taco Tuesday";
    break;
  case THURSDAY:
    message += "Pizza Night";
    break;
}
```

```
/*This expression version of the same code uses the -> to signify that only the code
on the right side of this label is what is executed if the case values match. When multiple
case labels are used, this expression supports comma-separated labels in a single case. */

String message = "My favorite days are for ";
switch(day) {
  case FRIDAY, SATURDAY, SUNDAY -> message += "Weekend Fun";
  case MONDAY, WEDNESDAY -> message += "Pasta Party";
  case TUESDAY -> message += "Taco Tuesday";
  case THURSDAY -> message += "Pizza Night";
}
```

### Java 13
The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/13/)

- **JEP 350 Dynamic CDS Archives**
  - [Documentation](https://openjdk.java.net/jeps/350)
  - Extend Application class-data sharing which allows dynamic archiving at the end of a Java application
  execution, dynamic archiving.
- **JEP 351 ZGC: Uncommit Unused Memory (Experimental)**
  - [Documentation](https://openjdk.java.net/jeps/351)
  - Enhance ZGC to return unused memory to the OS
- **JEP 353 Reimplement the Legacy Socket API**
  - [Documentation](https://openjdk.java.net/jeps/353)
  - A modern implementation to replace `java.net.Socket` and `java.net.ServerSocket` APIs
- **JEP 354 Switch Expressions (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/354)
  - In addition to JEP 325, a new **yield** statement was added based on feedback to replace a break
  statement with value, which would be used to yield a value from the switch expression.  
- **JEP 355 Text Blocks (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/355)
  - This preview feature is a multi-line string literal that avoids the need for most escape sequences.
  It formats the string automatically in a predictive way and provides the developer more control
  over the format.

##### Switch Expression Yield example
```
// yield statement on switch statements
int alpha = 10;
int beta = 15;
int total = switch(type) {
  case ADDITION:
    yield 15;
  case SUBTRACTION:
    yield 5;
  default:
    int result = 15 * 10;
    yield result;
}
```

```
// yield statement as a switch expression
int alpha = 10;
int beta = 15;
int result;
int total = switch(type) {
  case ADDITION -> 15;
  case SUBTRACTION -> 5;
  default -> {
    int result = 15 * 10;
    yield result;
  }  
}
```

###Java 14
The reference information for this is part of the OpenJDK website. [Reference](https://openjdk.java.net/projects/jdk/14/)

- **JEP 305 Pattern Matching for instanceof (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/305)
  - This introduces pattern matching for the instanceof operator. Instead of having to cast the type,
  the pattern is added to the common if declaration. See below for one example, but more are in the
  documentation.
- **JEP 343 Packaging Tool (Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/343)
  - A tool for packaging self-contained Java applications based on the JavaFX packager tool.
- **JEP 345 NUMA-Aware Memory Allocation for G1**
  - [Documentation](https://openjdk.java.net/jeps/345)
  - Improved G1 performance on large machines implementing NUMA-aware memory allocation
- **JEP 349 JFR Event Streaming**
  - [Documentation](https://openjdk.java.net/jeps/349)
  - Expose JDK Flight Recorder data for continuous monitoring.
- **JEP 352 Non-Volatile Mapped Byte Buffers**
  - [Documentation](https://openjdk.java.net/jeps/352)
  - New file mapping modes added to the JDK so the FileChannel API can be used to create MappedByteBuffer
  instances that refer to non-volatile memory.
- **JEP 358 Helpful NullPointerExceptions**
  - [Documentation](https://openjdk.java.net/jeps/358)
  - Improving the generated NullPointerExceptions by describing which variable caused it more precisely.
- **JEP 359 Records (Preview)**
  - [Documentation](https://openjdk.java.net/jeps/359)
  - Records provide a compact syntax for declaring classes with immutable data.
- **JEP 361 Switch Expressions (Standard)**
  - [Documentation](https://openjdk.java.net/jeps/361)
  - Now standard, switch expressions can be used, or the traditional switch pattern. Expression version
  will prepare for the use of pattern matching. This included to creation and use of a new statement
  called *yield*.
- **JEP 362 Deprecate the Solaris and SPARC Ports**
  - [Documentation](https://openjdk.java.net/jeps/362)
  - These ports are being deprecated and will eventually be removed.
- **JEP 363 Remove the Concurrent Mark Sweep (CMS) Garbage Collector**
  - [Documentation](https://openjdk.java.net/jeps/363)
  - This garbage collector was deprecated in JEP 291 and received no updates since to justify
  keeping it. No other garbage collectors are road-mapped for removal.
- **JEP 364 ZGC on macOS**
  - [Documentation](https://openjdk.java.net/jeps/364)
  - This Linux-based garbage collector is now supported on Mac for development.
- **JEP 365 ZGC on Windows**
  - [Documentation](https://openjdk.java.net/jeps/365)
  - This Linux-based garbage collector is now supported on Windows for development.
- **JEP 366 Deprecate the ParallelScavenge + SerialOld GC Combination**
  - [Documentation](https://openjdk.java.net/jeps/366)
  - This deprecation is not to remove it or deprecate any other garbage collector algorithm, but 
  requires a significant amount of maintenance effort.
- **JEP 367 Remove the Pack200 Tools and API**
  - [Documentation](https://openjdk.java.net/jeps/367)
  - This feature was deprecated in java 11 and is subject to removal with this jdk update.
- **JEP 368 Text Blocks (Second Preview)**
  - [Documentation](https://openjdk.java.net/jeps/368)
  - This is a second preview of this feature with 2 new escape sequences added that are specific to
  this feature.
- **JEP 370 Foreign-Memory Access API (Incubator)**
  - [Documentation](https://openjdk.java.net/jeps/370)
  - This api will allow java programs to safely and efficiently access foreign memory outside the
  java heap.

##### Pattern Matching for instanceof
```
// This basic if statement is what we typically do
if(obj instanceof String) {
  String something = (String) obj;
  // remaining code
}

// With Pattern matching, we no longer need to cast
if(obj instance of String something) {
  // remaining code
} else {
  // we can't use it
}
```

##### Records (Preview)
```
// This code is a typical data class. In fundamentals we talked about some of these methods
// but did not implement them in our data classes.
public class Something {
  private final String firstName;
  private final String lastName;
  private final long phone;
  
  public Something(String first, String last, long phone) {
    this.firstName = first;
    this.lastName = last;
    this.phone = phone;
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(firstName, lastName, phone);
  }
  
  @Override
  public boolean equals(Object obj) {
    if(this == obj) {
    
    } else if(!(obj instanceof Something)) {
      return false;
    } else {
      Something other = (Something)obj;
      return Objects.equals(firstName, other.firstName)
      && Objects.equals(lastName, other.lastName)
      && Objects.equals(phone, other.phone);
    }
  }
  
  @Override
  public String toString() {
    return "Something[first name = " + firstName + ", last name = " + lastName +
    " phone = " + phone + "]";
  }  
  
  // getter methods for the variables would go here.
}
```


```
// The following code is the Record version of the code above. The above
// code is auto-generated as a result.
public record Something(String firstName, String lastName, long phone) {  }

// This one line will generate the hashCode, equals, toString, constructor and getter methods.
// You can create an additional constructor that takes less arguments.
// Any additional methods or variables in records has to be declared static.
```

