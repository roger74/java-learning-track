# Assignments

Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module2_3.java. Create any additional files as needed for the task. Include a
main method to test your code.

### Task 1
Write a method that will use the Scanner class. Using a switch expression, validate a char value
against the below table of grades. Return the proper description. If an incorrect value is 
provided, return the following message *Not a valid grade* and have them begin again. Case sensitivity
matters with the Grade.

| Grade | Description |
|----|----|
| E | Excellent |
| V | Very Good |
| G | Good Job |
| A | Average |
| F | Failed |

### Task 2
Read through all the documentation provided about Records. Create an example of a record that has
4 parameters. Write an additional constructor that takes in 2 of the 4 parameters and utilize the
**this** keyword to pass the information to the auto generated record constructor.


### Task 3
Read through all the documentation in session 3 about Text Blocks. Provide 2 different examples in
methods for text blocks. One of the examples should have HTML in it.