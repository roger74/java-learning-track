# Java 8

In this module we are going to cover some aspects that were not part of the fundamentals course. We
will only be covering subjects that are not part of any future modules in this learning track. Some
topics in this session have only been briefly discussed in the fundamentals course, or were used but
not fully explained. A more detailed explanation will be in the below content.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session1_assignment.md).

---
### Type Casting
Not to be confused with primitive data type casting, **Type Casting** refers to the casting of objects.
In the fundamentals course we did small variations of this, but did not refer to it by its actual name.
When we create and object `House myHouse = new House()` we create a variable called myHouse which
is of type House. This how we refer to it as a reference type. Because of this **myHouse** holds a 
reference to House in memory rather than holding the object itself.

**Typecasting** is a way to change a class or interface from one type to another. Keep in mind this
only works if these types are part of a super class or implement the same interface. This may sound
like a concept that was done in the fundamentals class where you would use a parent variable with 
a child object. That is a form of upcasting or implicit casting.

```
// Upcasting
Condo myCondo = new Condo();
House myHouse = (House)myCondo;

// Downcasting
Condo myOtherCondo = (Condo)myHouse;
```
**Upcasting** in this case is the statement labels *myCondo* as a House type instead of a Condo type.
This doesn't change the original object as it only labels it as a different type. Because of this type
of casting, *myCondo* no longer has access to the methods and fields of Condo, but do of House.

**Downcasting** converts the type from the super class to the subclass. You will now gain access to
the subclasses methods and fields. This only works if the object originally came from the subclass.
If you try to downcast to an object that wasn't the original, you will get and exception. One way
to prevent this is the **instanceof** operator. This operator allows you to check if the object is
of the given type.

``` 
if(myHouse instanceOf Condo) {
    Condo myOtherCondo = (Condo)myHouse;
} 
```

### Autoboxing
In the Java language we typically use both reference types and primitive data types. There are
occasions where we need to convert a primitive data type to a reference type. This type of conversion
is called **Autoboxing**. Each primitive data type has a corresponding reference type. These
reference types are known as wrapper classes. Below is a table that shows both types:

| Primitive Type | Wrapper Class |
|----|----|
| int | Integer |
| long | Long |
| short | Short |
| char | Character |
| double | Double |
| float | Float |
| boolean | Boolean |
| byte | Byte |

Collections, which we reviewed in the previous module is a good example of using the Wrapper classes.

```
Integer beta = new Integer(19);

// we can also do this without using the new keyword
Integer charlie = 15;

// Unboxing is coverting from Wrapper back to Primitive
int alpha = charlie;
```


### Lambda Expressions
This kind of expression is a short block of code that takes in parameters and returns a value. They
are similar to method, but don't require a name and can be used in a method.

` parameter -> expression `

These type of expressions cannot contain variables, assignments or control flow statements. You
will need to do a code block with curly braces where you can use a return statement as needed. The
symbol of ` -> ` is what signifies and lambda expression.

Lambda expressions can be stored in variables if its type is an interface that has only 1 method.
The expression should have the same number of parameters and return type as the method.

```
interface StringStuff {
    String update(String value);
}

public class SomeClass {
    void someMethod() {
        StringStuff excite = (happy) -> happy + "!";
        StringStuff question = (what) -> what + "?";
        someFormat("Java is cool", excite);
        someFormat("Java is cool", question);
    }
    void someFormat(String val, StringStuff format) {
        String end = format.update(val);
        System.out.println(end);
    }
}

Result
Java is cool!
Java is cool?
```

For more information on this topic, visit [Oracle's Tutorials](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html)

### Functional Interfaces
A Functional Interface is an interface with only 1 abstract method. Java 8 Introduced `java.util.function`
, which contains functional interfaces that are expected to be commonly used. There are more than
the below defined interfaces. Refer to [Documentation](https://docs.oracle.com/javase/8/docs/api/java/util/function/package-summary.html)
- **Predicate<T>**
  - A Boolean-valued property of object
- **Consumer<T>**
  - An action to be performed on an object
- **Function<T,R>**
  - A function transforming a **T** to an **R**
- **Supplier<T>**
  - Provide an instance of **T** 
- **UnaryOperator<T>**
  - A function from **T** to **T**
- **BinaryOperator<T>**
  - A function from **(T,T)** to **T**
  

### The :: Operator
This operator is a method reference and can refer to a static method, instance method, or constructor.
Using this operator, you only refer to the method or lambda expression, so the parenthesis is not
required. By Lambda expression, we can refer to the method reference using this operator rather than
the expression itself. This type of operator is also used in conjunction with a Functional Interface.


```
class SomeClass {
  public double square(double value) {
    return Math.pow(value, 2)
  }
}

SomeClass sample = new SomeClass();
Function<Double, Double> value = SomeClass::square;
double amount = value.apply(10); // apply is the method in Function<T,R>
```

### Date Time
There are a series of date time classes that exist in the java language prior to Java 8. There are
java packages that contain some form of date and time.
- java.time
  - Contains the Java 8 Date and Time classes
- java.util
  - Contains the legacy Java Date API
- java.sql
  - Contains the legacy Java Date API
- java.text
  - Contains the classes for Formatting Date and Time.

**Date Time APIs** in Java 8 help reduce the need to use the legacy classes. The below classes from
this Java 8 Date Time API table shows what is available.

| java.time class | Description |
|----|----|
| Clock | Provides current time, date in any time-zone. Is optional and allows for testing for other time zones. |
| format.DateTimeFormatter | Defined formatters along with allowing custom formatters. Including parsing and formatting values. |
| Duration | Difference between two instances measured in seconds or nanoseconds. Includes methods for converting to days hours or minutes |
| Instant | Represents an instant in time as a nanosecond. This timestamp is based around the EPOCH time and is a positive after the EPOCH or a negative value after |
| LocalDate | Defined dates without time based on the year-month-day of the ISO calendar |
| LocalDateTime | Represents both date and time without a timezone. A combination of the *Local Date* and *Local Time* |
| LocalTime | Defined as time only and can represent a day. |
| OffsetTime | Represents time with a timezone offset from Greenwich/UTC without a time zone id |
| OffsetDateTime | Represents a date and time with a timezone offset from Greenwich/UTC without a time zone id |
| Period | Defined as a difference between dates in date-based values |
| ZonedDateTime | Combines *LocalDateTime* with the *ZoneId class* |
| ZoneId | States a time zone id and rules for converting and *Instant* and *LocalDateTime* |
| ZoneOffset | Timezone offset from Greenwich/UTC time |

Refer to this [article](https://www.baeldung.com/java-8-date-time-intro) for more information on this
topic. Bookmark it as you will have an assignment/lab using Date Time.

```
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class SomeClass {
    static void someMethod() {
        LocalDateTime dateObj = LocalDateTime.now();
        System.out.println("Unformatted Date " + dateObj);
    
        // format the date time using a pattern
        DateTimeFormatter myFormatter = DateTimeFormatter.ofPatter("dd-MM-yyyy HH:mm:ss");
        String dateFormatted = dateObj.format(myFormatter);
        System.out.println("Formatted Date " + dateFormatted);
    }
    static void main(String[] args) {
        someMethod();
    }
}
```

To read more information on the **DateTimeFormatter** patterns and information, refer to this
[Oracle](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html) documentation.
