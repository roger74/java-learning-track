# Assignments

Complete the following code tasks to reinforce the session 1 content. For this session you will
create a file called module2_1.java. Include a main method to test your code.

### Task 1
Utilizing the Module 1's session 3 assignment task 1, create 2 examples of both upcasting and
downcasting. Ensure you have the proper imports of those classes.

### Task 2
Create 2 functional interfaces based on the below calculations. Write methods that will utilize a
lambda expression to consume the interfaces. Be sure to print and test your results.
- calculation 1
  - pythagorean theorem
- calculation 2
  - conversion of feet to inches

### Task 3
Utilizing the below EPOCH times, write a method that will display the results using the following
pattern: Mon Jan 01, 2021
- EPOCH Time 1
  - 819921912
- EPOCH Time 2
  - 1118829912
- EPOCH Time 3
  - 1634846819