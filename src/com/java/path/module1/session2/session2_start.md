# Operators & Control Flow

In this session will review the Operators and Control Flow portions of the material from the Java
Fundamentals course. As for Operators we will focus on only the operators that are more commonly
used. For the ones we don't cover here, you should still understand how they work.

Study the information below. Take notes. Once complete, begin your [Session Assignment](session2_assignment.md).

---
### Operators
We will be looking at the following sections of the Operators Slide Deck and example content.
- Arithmetic
- Assignment
- Relational
- Logical
- Increment & Decrement
- Ternary Operator

##### Arithmetic Operators
These operators are used for mathematical expressions in the same way used in Algebra and/or basic
math.
```
int alpha = 10;
int beta = 20;
```

| Operator | Description | Example |
|----------|-------------|---------|
| Addition | Adds values on either side of the operator | `total = alpha + beta` |
| Subtraction | Subtracts right hand operand from left hand operand | `total = alpha - beta` |
| Multiplication | Multiplies values on either side of the operand | `total = alpha * beta` |
| Division | Divides left hand operand by right hand operand | `total = beta / alpha` |
| Modulus | Divides left hand operand by right hand operand and returns remainder | `total = beta % alpha` |
| Increment | Increases the value of the operand by 1 | `beta++` or `++beta` |
| Decrement | Decreases the value of the operand by 1 | `alpha--` or `--alpha` |

```
class Example {
    static void main(String[] args) {
        int num1 = 10;
        int num2 = 15;
        
        int total = num1 % num2;
        System.out.println("modulus total = " +total)
    }
}

Output
modulus total = 10
```

##### Assignment Operators
These operators are used for assigning value to an operand. An operand represents the variable on
either side of an operator. We will not be covering the Bitwise or shift types. For the exception
of the first type, the remaining types re-assign the value to an existing variable, which is basically
shorter math.
```
int charlie = 5;
int delta = 12;
```

| Type | Description | Example | Equivalent |
|------|-------------|---------|-------|
| = | Assigns values from right to left operand. |`total = charlie + delta`| basic equivalent |
| += | Adds right to left and assigns the result to the left operand. |`charlie += delta`|`charlie = charlie + delta`|
| -= | Subtracts right to left and assigns the result to the left operand. |`charlie -= delta`|`charlie = charlie - delta`|
| *= | Multiply right to left and assigns the result to the left operand. |`charlie *= delta`|`charlie = charlie * delta`|
| /= | Divide right to left and assigns the result to the left operand. |`charlie /= delta`|`charlie = charlie / delta`|
| %= | Modulus right to left and assigns the result to the left operand. |`charlie %= delta`|`charlie = charlie % delta`|

##### Relational Operators
These operators are used to check the relationship between two variables. The result is either
true or false. These are commonly used with Control Flow statements.
```
int echo = 20;
boolean canBe;
```

| Operator | Result | Example |
|------|-------------|--------|
| `==` | Equal to | `canbe = (25 == echo)` |
| `!=` | Not Equal to | `canbe = (25 != echo)` |
| `>` | Greater than | `canbe = (25 > echo)` |
| `<` | Less than | `canbe = (25 < echo)` |
| `>=` | Greater than or Equal to | `canbe = (25 >= echo)` |
| `<=` | Less than or Equal to | `canbe = (25 <= echo)` |

##### Logical Operators
These operators like relational operators are used primarily in Control Flow Statements. The
Logical Operator allows for two or more expressions to be evaluated.

```
int fox = 10;
```

| Operator | Result | Example |
|------|-------------|--------|
| `&&` | And | `if(fox > 8 && fox < 12) { // logic goes here }` |
| &#124; &#124; | Or | `if(fox > 8 `&#124; &#124;` fox < 12) { // logic goes here }` |
| `!` | Not | `canbe = (25 > echo)` |

##### Increment & Decrement
These operators consist of a variable with either a pre operator or a post operator.
With the pre operators the value assigned to the variable will either increment or
decrement before a result is computed. With the post operators the value is computed
then the increment or decrement will complete. Increment or Decrement operators can 
only be applied to primitive variable types. Any variable that cannot change because
it is a constant or is declared as final, cannot use these types of operators. You
also cannot nest both operators (++variable++).

`
    int golf = 10;
`

| Operator | Result |
|------|-------------|
| `++golf` | pre-increment |
| `golf++` | post-increment |
| `--golf` | pre-decrement |
| `golf--` | post-decrement |


```
// Increment Example
void exampleIncrement() {
    int home = 10;
    
    int jump = home++;
    System.out.println(jump) // answer is 10
    
    int car = ++home;
    System.out.println(car); // answer is 12
}
```

```
// Decrement Example
void exampleDecrement() {
    int value = 10;
    
    int total1 = value--;
    System.out.println(total1) // answer is 10
    
    int total2 = --value;
    System.out.println(total2); // answer is 8
}
```

##### Ternary Operator
This operator is the only conditional operator that takes up 3 operands. This operator is a replacement
for an if/else control flow statement. We will discuss this in more detail in Control Flow Statements.

| Operator | Result |
|------|-------------|
| `?:` | ternary Operator if/else short-cut |


### Control Flow Statements
Control Flow statements help break up the flow of code execution by employing 
decision-making, looping and branching. This allows your program to execute blocks of 
code based on certain conditions.

We will begin this part of the session with:
- Decision Statements
  - if
  - if else
  - if else chain
  - ternary if
  - switch
- Looping Statements
  - while
  - do while
  - for
  - enhanced for (foreach)
- Branching Statements
  - break
  - continue
  - return

##### If Statements
An **if** statement executes a block of code if the boolean condition is true. 
The **else** statement is an optional statement that follows if the boolean 
condition is false.
- Conditional operators & logical operators can be used with if statements
- You can chain multiple if/else statements to create a chain.
- You can have multiple if statements that are not in a chain.

```
// basic if
if(condition) {
    // true condition code
}

// if else
if(condition) {
    // true condition code
} else {
    // false condition code
}

// if else chain
if(condition) {
    // true condition code
} else if(condition) {
    // else true condition code
} else {
    // false condition code
}

```

##### Ternary If
This type of statement is a replacement for an one-linear type if/else statement. It utilizes the ?: operator

`variable = Expression1 ? Expression2 : Expression3`

```
int val2 = 10;
int val3 = 12;
int max;

max = (val2 > val3) ? val2 : val3;
System.out.println("max value is" + max); 
```


##### Switch Statements
The switch lets you branch execution of code based on a value of an expression. 
It utilizes a **case** statement and will execute that code if that case statement is 
equal to the expression of the switch. A **default** statement is used if none of the 
case statements are true. The switch statement can be made up of primitive data types:
byte, short, int & long. Also works with enum types, String and Wrapper classes like
Byte, Short, Int and Long.

```
switch(expression) {
  case value1:
    //code
    break; //optional
  case value2:
    //code
    break; //optional
  case value3:
    //code
    break; //optional
  default:
    //code executes if no cases are matched
    break; //optional
}
```

### Looping Statements
Looping statements all a block of code to run while certain conditions are true. When the condition becomes
false, the loop will end. The conditions consist of a Relational Operator over a number or collection. There 
are 4 different types of Looping statements:
- While
- Do While
- For
- Enhanced For (Foreach)

##### While Loops
A while loop will evaluate the condition before the body of code is executed. It is made up of 3 parts
1. Initialized Value
2. Condition
3. Increment/Decrement

```
int value = 0;
while(condition true) {
  // Code executes
  value++ // increment
}
```

##### Do While Loop
A do while loop will execute the code once before evaluating the condition. It is made up of 3 parts.
1. Initialized Value
2. Condition
3. Increment/Decrement

```
int value = 0;
do {
  // Code executes
  value++ // increment
} while(condition true)
```

##### For Loop
The for loop provide a compact way to iterate of a range of values. The for loop takes the initialization,
condition and iteration from a While loop and compacts it in a more readable way. Like the While loop the
expression is evaluated before the block of code is executed.

`for(initialization; condition; iteration)`

```
for(int i=0; i<10; i++) {
  // Code executes
}
```

##### Enhanced For Loop
The enhanced for loop works with Collections and iterates through the collection without the need of the three
items that make up the other looping statements. It consists of 3 different requirements:
1. The Collection
2. Keyword in
3. A single variable of the same type as the collection

`for(single variable in collection)`

```
String hotel = "JavaFundamentals";
for(char item in hotel) {
  System.out.println(item);
}
```

### Branching Statements
These statements allow the flow of a code block to jump from one part to another. These type of 
statements are used with control flow statements or a method declaration. There are three main branching
statements:
- break
- continue
- return

##### Break Statements
The break statement ends the execution of block of code. Its most common use is with switch statements, but
can also be seen used within an if statement enclosed within a looping statement.

##### Continue Statements
The continue statement will end an iteration of a loop along with any statements after and begin the next 
iteration. Commonly used in looping statements and within an if statement.

##### Return Statements
The return statement is used with methods primarily and exits a method with a non-void return type. This statement
also is used in conjunction with a switch statement in place of a break statement when the data of that switch
needs to exit the method.

```
void basicBranchExample() {
  for(int i = 0; i < 10; i++) {
    if(i == 2) {
      continue;
    }
    if(i == 4) {
      System.out.println("Loop Breaks");
      break;
    }
    System.out.println(i);
  }
  System.out.println("End of loop");
}
```
