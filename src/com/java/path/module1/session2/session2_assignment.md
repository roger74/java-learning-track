# Assignments

Complete the following code tasks to reinforce the session 2 content of Operators & Control Flow.
Create a project on GitHub that will represent all your code puzzles for the Java Fundamentals 
Review. Include a main method to test your code. Create at least 1 class file to represent your 
session unless otherwise instructed. For this session you will create a file called module1_2.java.



### Task 1
Given the formula below, write a method that will include the formula and calculate the results. 
This method will take two parameters and no return type. When the calculation is complete, print 
out the results. Verify your results by calculating the answers on paper.

![Task 1 Formula](session2task1b.png)

Use the following parameter values to test your method.
- a = 10, b = 20
- a = 5, b = 15
- a = -24, b = 30
- a = 20, b = -10
- a = 20, b = 10


### Task 2
Write a method that will output the results of a triangle of numbers. Below are the triangles you
will need to output. Use control flow statements to complete this task.

1. ```
    1
   101
   10001
   1000001
   100000001
   ```

2. ```
    5
   45
   345
   2345
   12345
   012345
   ```
   
### Task 3
Using the Ohm's law table below, write a function that will take in 3 parameters. Each parameter
will represent one of the variables in the table. Using Operators and Control Flow, calculate 1 of
the versions of the formula if one of the parameter values is less than 0. This parameter
represents the unknown value. Print the results of all the values. If all the values are greater 
than 0, just print all the values. If more than 1 of the parameters is less than 0, print 
out *can not calculate, not enough information*. Include the name of the type with your values.

| V = Voltage | I = Current | R = Resistance |
|-----|-----|-----|
| Unknown Value | Formula |
| Voltage | V = I * R |
| Current | I = V / R |
| Resistance | R = V / I |

