# Collections
The Collection's framework is a group of classes and interfaces that provided by java for 
organizing and storing data. They are containers, an object that groups multiple elements into a 
single unit and are used to store, retrieve, manipulate, and communicate data.

Typically, they represent data items that form a natural group, such as a poker hand (a collection
of cards), a mail folder (a collection of letters), or a contacts list (a mapping of names to 
phone numbers).


Study the information below. Take notes. Once complete, begin your [Session Assignment](session5_assignment.md).

### Benefits of the Collection's Framework
- **Reduces programming effort**
  - By providing useful data structures and algorithms, the Collections Framework frees you to 
  concentrate on the important parts of your program rather than on the low-level “plumbing” 
  required to make it work.
- **Reduces effort to design new APIs**
  - This is the flip side of the previous advantage. Designers & implementers don't have to reinvent
  the wheel each time they create an API that relies on collections, instead, they can use standard
  collection interfaces.
- **Fosters software reuse**
  - New data structures that conform to the standard collection interfaces are by nature reusable.
  They same goes for new algorithms that operate on objects that implement these interfaces.

### Types of Collections
There are a good number of collection interfaces and classes to work with your data. We will
discuss those shortly. There is 2 specific types of collections that we need to discuss.
- Non-Generic
- Generic

##### Non-Generic
A non-generic type of collection is a collection that does not specify the **type** of data it
contains. This makes it difficult to manage because it only takes 1 mis-match to cause an exception.

`ArrayList myList = new ArrayList();`

As you can see from the above collection class, that it doesn't specify a type. This type of will
require a casting when you need to retrieve any data and the type-safe check is done at runtime.

##### Generic
A generic type of collection is a collection that specifies the type using the `<>` in the
declaration. The `<>` refers to a reference type, so for primitive types such as int or long, you
have to call their class equivalent; Integer, Long.

`ArrayList<String> myList = new ArrayList<>()`;

With the reference type defined, a generic collection is type safe, requires no type casting and is 
checked for type safety at compile-time.

### Collection Interfaces
There are 4 Interfaces that extend from Collection:
- **Set Interface**
  - An unordered collection that does not maintain any order while storing elements. It also does
  not allow duplicate elements.
- **List Interface**
  - An ordered collection that allows duplicates. Elements are stored in sequential order and can
  be accessed using the index value.
- **Queue Interface**
  - A collection used to hold multiple elements prior to processing. This type of collections provides
  additional insertion, extraction and inspection operations. This collection typically, but not
  always orders elements in a FIFO(first-in, first-out) manner.
- **Deque Interface**
  - Like the queue interface this collection is used to hold multiple elements prior to processing.
  This also includes the additional insertion, extraction and inspection operations. What sets it
  apart is it can be used for both FIFO and LIFO(last-in, last-out). This allows new elements to be 
  added and retrieved at both ends of the collection.

There is another interface that is a collection, but does not derive from the Collections Interface.
- Map
  - A collection object that maps keys and values. The keys cannot contain duplicates.


##### Classes using Set Interface
There are three classes that implement the Set Interface. They are:
- HashSet
- LinkedHashSet
- TreeSet

There are various methods for the Set Interface.

| Method | Description | 
|----|----| 
| `add()` | Used to add a specific element to the collection. It will only add if it is not already in the collection |
| `addAll(collection)`  | Used to add all of a collection to to this collection. Elements are added randomly  |
| `clear()` | Used to remove all elements from the collection without deleting the collection. |
| `contains()` | Used to check if the an element is in the collection. Returns true of false|
| `containsAll(collection)` | Used to check if all the elements are in a set collection. Returns true or false|
| `hashCode()` | Used to get the hashcode value for the instance of the Set. Returns an integer value that is the hashcode value.|
| `isEmpty()` | Used to check if the collection is empty|
| `iterator()` | Used to return an iterator of the set. Elements are returned in random order|
| `remove()` | Will remove a specific element from the collection|
| `removeAll(collection)` | Will remove all the elements from the collection|
| `retainAll(collection)`| Used to retail all elements from the collection. Returns true if the set changed from the call.|
| `size()` | Used to return the size of the collection as an integer value|
| `toArray()` | Used to return an array of the same elements.|

For more information about the Set Interface, visit [Oracle's Website](https://docs.oracle.com/javase/7/docs/api/java/util/Set.html).

The **HashSet** class collection contains unique elements only, so no duplicates are allowed. Its data
structure is a hashtable. Null elements are allowed and the hashset implements the Serializable
& Cloneable interfaces along with the Set Interface. The hashset relies on the Iterator class
in order to cycle through the collection using the iterator method.

`HashSet<String> list = new HashSet<>()`

```
void someMethod() {
  HashSet<String> list = new HashSet<>();
  list.add("Java");
  list.add("fundamentals");
  list.add("Module");
  
  Iterator<String> it = list.iterator();
  while(it.hasNext()) {
    System.out.println(it.next());
  }
}
```

##### Classes using the List Interface
There are 4 classes that implement the list interface
- ArrayList
- LinkedList
- Vector
- Stack

As you can see one of the classes is called Stack. Not to be confused with the memory stack that
we discuss in conjunction with the memory heap.

There are various methods for the List Interface.

| Method | Description | 
|----|----| 
| `add(Object obj)`| Overloaded method for adding objects to the collection. Can specify exact location using an alternative method in the collection.|
| `addAll(collection)` | Used to add elements from one collection to another of the same object type. An alternative method allows a starting position for the insert. |
| `get(int index)` | Used to return the value of a given element based on its index in the collection. |
| `indexOf(Object obj)` | Used to return the first instance of an object in the collection. A 1 is returned if the element is not in the list|
| `lastIndexOf(Object obj)` | Same as the indexOf method, but returns the last instance.|
| `listIterator()`| Returns the iterator to the start of the invoking list. An alternative method can return beginning from a specified index. |
| `remove(int index)` | Removes an element from the collection based on its position in the collection. The collection is decremented by one. |
| `set(int index, Object obj)` | Assigns the object to the specific index in the collection|
| `subList(int start, int end)` | Returns a list the includes the elements from the start and end values by index. |


For more information on this interface, check out [Oracle's Website](https://docs.oracle.com/javase/8/docs/api/java/util/List.html).

The **ArrayList** class collection is similar to an array accept it is dynamic. This basically means you
can add and remove elements of an arraylist. This also means that it is slower when it comes to
performance. You can have duplicate records in an arraylist and this collection is not synchronized.
You can declare them with or without Generics.

`ArrayList<Integer> ages = new ArrayList<>();`

```
void someMethod() {
  ArrayList<Integer> ages = new ArrayList<>();
  ages.add(48);
  ages.add(22);
  ages.add(18);
  ages.add(13);
  ages.add(46);
  
  for(int i = 0; i < ages.length; i++) {
    System.out.printin(ages.get(i).toString());
  }
}
```

##### Classes using the Queue Interface
There are two main classes that implement the Queue interface.
- LinkedList
  - Also implements the List Interface
- PriorityQueue

There is also an AbstractQueue class which implements the Queue interface, but as its name implies,
it is Abstract.

There are various methods for the Queue Interface.

| Method | Description | 
|----|----|
| `add()` | Used to add elements at the end of the queue. Returns true if successful and an exception if it is not successful. |
| `offer()` | Same as the add method except it returns true if successful or false if is not successful.|
| `remove()` | Returns to the first element in the collection and removes it. Throws an exception if the collection is empty. |
| `poll()` | Same as the remove method except that it returns null if the queue is empty.|
| `peek()` | Returns the 1st element in the queue without removing it. Returns null if the queue is empty.|
| `element()`| Same as the peek method except that it throws an exception if the queue is empty.|
| `size()` | returns the number of elements in the queue as an integer. |
| `isEmpty()` | Returns true if the collection is empty, otherwise it returns false. |

For more information on this interface, check out [Oracle's Website](https://docs.oracle.com/javase/tutorial/collections/interfaces/queue.html).

The *PriorityQueue* class collection inherits from the AbstractQueue class and therefore does not order
elements in a FIFO manner. They are instead processed based on their priority. This means they are
based on the priority heap, ordered based on natural ordering or a comparator at construction time.
The priorityqueue also does not permit nulls, are considered unbound queues.

`PriorityQueue<Long> queueList = new PriorityQueue<Long>();`

```
void someMethod() {
  PriorityQueue<Long> queueList = new PriorityQueue<Long>();
  
  // add items
  queueList.add(123456789);
  queueList.add(112345678);
  queueList.add(111234567);
  
  // print the top element
  System.out.println(queueList.peek());
}
```
