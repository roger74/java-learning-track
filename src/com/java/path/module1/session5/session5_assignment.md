# Assignments

Complete the following code tasks to reinforce the session 5 content. For this session you will
create a file called module1_5.java. Include a main method to test your code. 

You will also need to create a second class file with an object of your choice. It should have
the following requirements:
- two attributes with setter methods
- constructor that takes 2 parameters
- two methods for this class that are not the setters.

You will use the class file as the object of the tasks below.

### Task 1
Create a method and utilize the TreeSet Collection. Utilize your created object class in the 
example. Use a minimum of 4 set interface methods in your example. Include comments where needed.

### Task 2
Create a method using either the Vector or Stack collections. Utilize your created object class in
example. Use a minimum of 4 list interface methods in your example. Include comments where needed.

### Task 3
Create a method using the PriorityQueue collection. Using a String object insert the last 10 winning
World Cup Teams as elements into your collection. Iterate through the collection to print
the results.
