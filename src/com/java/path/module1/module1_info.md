# Java Fundamentals Review

In this fundamentals review we will go over various topics from the
original java course, and expand a little on areas that were not. Their
will be 5 different sessions as part of this module. Each session in this
module will have code labs to reinforce the content. These code labs will
need to be in a project and a GitHub repository. Additional requirements
such as pull requests, peer reviews or mentor discussions may be part of
these sessions. We will explore the following sessions below.

#### [Java Standards](session1/session1_start.md)
#### [Operators & Control Flow](session2/session2_start.md)
#### [Object Oriented Programming](session3/session3_start.md)
#### [Exceptions & Exception Handling Handling](session4/session4_start.md)
#### [Collections](session5/session5_start.md)