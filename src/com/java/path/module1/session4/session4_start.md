# Exceptions & Exception Handling
To handle runtime errors so the normal flow of an application can be maintained, exception handling
very powerful. Exception Handling in Java is a mechanism to handle runtime errors. There is a 
hierarchy for exception handling. It begins with **Throwable** (java.lang.Throwable). This parent
exception is followed by the following:
- Error (java.lang.Error)
- Exception (java.lang.Exception)

Study the information below. Take notes. Once complete, begin your [Session Assignment](session4_assignment.md).

___

### Types of Exceptions
There are Three types of exceptions:
- Checked Exceptions
- Unchecked Exceptions
- Error (also considered unchecked)

##### Checked Exceptions
These types of exceptions are checked by the compiler at compile time. They cannot be ignored and
should be handled by the programmer. Examples are:
- IOException
- SQLException
- FileNotFoundException

##### Unchecked Exceptions
These type of exceptions occur at the time of execution. Known as runtime exceptions. They are 
ignored by the compiler. Examples are:
- ArithmeticException
- NullPointerException
- ArrayIndexOutOfBoundsException

##### Error
This type of Exception is not know by the compiler. It occurs at runtime and is impossible to
recover from. They are typically caused by the Environment in which the application is running.
Some examples of this type are:
- OutOfMemoryError
- VirtualMachineError
- AssertionError
- StackOverFlowError

### Exception Handling
Java Exception handling is managed by 5 keywords
- try
- catch
- finally
- throw
- throws

##### Try Block
The **try block** contains set of statements where an exception can occur. A try block is always 
followed by a catch block, which handles the exception that occurs in associated try block. A try 
block must be followed by catch blocks or finally block or both.

```
try {
    // code that could cause an exception
}
```

##### Catch Block
A **catch block** is where you handle the exceptions, this block must follow the try block. A single 
try block can have several catch blocks associated with it. You can catch different exceptions in 
different catch blocks.

When an exception occurs in try block, the corresponding catch block that handles that particular 
exception executes. For example if an arithmetic exception occurs in try block then the statements 
enclosed in catch block for arithmetic exception executes.

Because you can have multiple catch blocks defined for a try block, the most likely caused catch 
should come before the most generic as they are written. While you can use the most generic
**Exception** type, it is discouraged in favor of a more specific exception catch. 

```
try {
    // code that could cause and exception
} catch(exception(type) e(object) {
    // code to handle the exception
} catch(exeption(type) e(object) {
    // code to handle the exception
}
```

```
class SomeClass {
    House ranch;
    
    public void exampleException() {
        House[] names = {new House(), new House(), ranch};
        try {
            System.out.println(names[3].getWindowSize());
        } catch (ArrayIndexOutOfBoundsException ex) {
           System.out.println(ex.getMessage());
        } catch (NullPointerException ax) {
           System.out.println(ax.getMessage());
        }
    }
}
```

##### Finally Block
A **finally block** contains all the crucial statements that must be executed whether exception 
occurs or not. The statements present in this block will always execute regardless of whether 
exception occurs in try block or not such as closing a connection, stream etc. Java finally block
follows try or catch block. The **finally block** in java is optional.

```
try {
    // code that could cause and exception
} catch(exception(type) e(object) {
    // code to handle the exception 
} finally {
    // code to always execute
}
```

##### Throw
This keyword is used to throw an exception explicitly. You can throw checked or unchecked exceptions
using this keyword.

```
throw new ArithmeticException("math error")
```

```
void verifyId(int age) {
    if(age < 21) {
        throw new ArithmeticException("Person not old enough to drink alcolhol");
    } else {
        System.out.println("Person can have alcolhol");
    }
}
```

##### Throws
Any exception that is thrown directly from a method uses the **throws clause**. This throw will
produce the specified exception if the contents of the method causes it. It does not handle the
exception.

```
void someMethod() throws NullPointerException {
    // code
}
```