# Assignments 

Complete the following code tasks to reinforce the session 4 content. For this session you will
create a file called module1_4.java. Include a main method to test your code.

### Task 1
Using the Scanner class, write a method to ask the user to provide a year. Using a try/catch, take 
the input and parse it to an int. Take that value and determine if it’s a leap year. Be sure to
validate if the input has the size of 4 before checking to see if it’s a leap year. If it’s not 4 
digits, tell the user that they did not provide a valid year. Test the catches by intentionally
trying to cause an exception.

### Task 2
Write a method that will use the **throws** keyword. When coming up with the code for this method, set
it up in such a way to pass without exception, but with a single comment, have it throw the
exception to ensure it works properly.

