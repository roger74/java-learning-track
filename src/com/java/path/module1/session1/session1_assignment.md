# Assignments

### Reading
Utilizing Google's [Java Style Guide](https://google.github.io/styleguide/javaguide.html) Read
the following sections after completing the [Session1 Start](session1_start.md):

- Section 4.6 Through 4.8.8 on Formatting
- All of Section 2 on Source file basics
- All of Section 3 on Source file structure
- All of Section 6 on Programming Practices