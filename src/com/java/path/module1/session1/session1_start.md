# Java Standards

In this session we will go over Java coding Standards and Practices. We
will be utilizing Google's [Java Style Guide](https://google.github.io/styleguide/javaguide.html)
as part of this session. This is because Oracle's style guide has not been updated for a really 
long time. We won't be covering all the material in the document, so you should bookmark it. 

Below is a list of the different sections we will go over in this session.
- Naming Guidelines
- Formatting

Included with this session will be additional reading. Refer to the [Session1 Assignment](session1_assignment.md)
for additional reading. Be sure to take notes on all reading material.

---
### Naming Guidelines
We will begin with some common information about all naming guidelines. All identifiers
will consist of letters and numbers with some cases where underscores are used.
The numbers and letters identify as ASCII and no other style. Things like special
prefixes and/or suffixes are not used.

##### Package Names
Package names are always lowercase and typically made up of 3 words separated
by a period. In a lot of cases they reflect an inverted url style. An example
is a website like *something.com* and the name of the company would produce
a package name such as *com.company.something*. Do not use underscores with package
names. Choose your names carefully. When creating secondary package names inside your
IDE, be sure to do it from the src folder and always maintain the first 2 parts of 
the package name and only change the third one; *com.company.other*

These package names are folder structure, so use them for organizing your code files and
let the last word in the package name reflect your organization. If you need to maintain
a three word package name, you can add a fourth name as the organized portion. Only do this
if you are going to have multiple package names in your application


##### Class Names
All class names are written in a case called **PascalCase**, or **UpperCamelCase**. Both
of these are the same syntax. The *pascal case* reference name is from an older programming language.
Class names are based on nouns and begin with a capital letter. An example is **House**, **MotorBike**,
or **ImmutableList**. You will notice that the two of them are made up of more than one word. When
creating classes with more than one word, each word **must** be capitalized. No underscores are used
in class names.

Interface names can also be nouns, but in some cases can also be adjectives. Test classes are
named starting with the class they are testing followed by the word **Test**.

##### Method Names
All method names are written in a case called **camelCase** or **lowerCamelCase** depending on
when you studied the language. Both are the same syntax. Method names are typically verbs and
begins with a lowercase letter. If the method name consists of more than one word however, each word
after the first word is capitalized. Method names also do not use underscores, unless the are
test methods. Examples are: **sendMessage**, **setFirstName**

JUnit test methods may have underscores between the method being tested and its state. There is
no specific way that a test method needs to be named, so take this as a general rule and adjust
based on the development studio you are working at, if they have a specific syntax.

##### Constant Names
All constants use UpperCase style convention. This means the word is in all capital letters. Any
additional words that may make up a constant name should be separated by underscores and also be
in all capital letters. Constants are typically nouns. Constants are made up of static final 
fields. This includes Strings, primitives, and immutable collections of immutable types. 
If any field created using these begin to create an instance of observable state that changes, 
it is not a constant. Examples

```
// Constants
static final int ID_NUMBER = 2345;
static final ImmutableList<String> GUEST_LIST = ImmutableList.of("Roger", "Kristi", "Matthew", "Jessica", "Emily");

// Not Constants
static String something = "missing-final";
final String other = "missing-static";
static final Set<String> comicCollection = new HashSet<String>();
```

##### Non-constant Field Names
##### Parameter Names
##### Local Variable Names
All of these name types are written in a case called **camelCase** or **lowerCamelCase** 
depending on when you studied the language. **Non-constant field names** are typically nouns.
**Parameter Names** should avoid using single character names in methods. **Local variable names**
when set as final and immutable are not considered constants and should not be styled as such.

##### Type variable names
These can follow one of two styles. The first one is single capital letter, with optional single
numeral. Examples are *E, T, X, T2*. The second is the name of the class followed by the letter T.

---
### Formatting
As the name implies there are certain aspects of coding that require correct formatting. This
is referring to things like curly braces and syntax that is not naming.

##### Optional Curly Braces
This refers to statements such as `if`, `else`, `for`, `do`, and `while`. There is a syntax in
which you can decide to not uses curly braces. These are when you have an empty body or a single
statement. It is best to still use curly braces in these cases as it is easier to identify. You 
may encounter these on exams without curly braces.

##### Non-Empty blocks
When creating non-empty blocks, you should follow the K&R style (Kernighan & Ritchie Style) or
*Egyptian Brackets*. The following rules of this style apply
- No line break before the opening curly brace.
- Line break after the opening curly brace.
- Line break before the closing curly brace.
- Line break after the closing brace on method bodies, constructors or class.

```
void method() {
    if(expression) {
        //code
    }
}

```

##### Empty Blocks
These types of blocks may follow the K&R style, or may be closed immediately after it is opened.
The exception to this would be if it is part of a multi-block statement.
```
// Acceptable
void someMethod() {}
// or
void someOther() {
}

// Not Acceptable
try {
    something();
} catch(Exception ex) {}
```

##### Block Indention
For every new block or block construct, the indent should increase by 2 spaces. When the block ends
, the indent returns to the previous indent level. This applies to code and comments

##### Statements
Each statement should be followed by a line break.

##### Column limit
The column limit for java code is 100 characters. Any line that may exceed this should line-wrap.
There are some exceptions to this standard:
- Long URL in Javadocs or a JSNI method reference
- package and import statements
- Command lines in a comment that may be copied into a shell.

#### Line-Wrapping (where to break)
While line wrapping may be a result of staying within the column limit, the developer could choose
to line-wrap a set of code that would fit within the column limit. Below are how we should handle
line-wrapping as far as where to break.
1. A break should appear before the symbol of a non-assignment operator. This should also apply to
    - dot(period) separator `(.)`
    - two colons of a method reference `(::)`
    - an ampersand in a type bound `(<T extends Home & Car>)`
    - a pip character in a catch block `(catch(NullReferenceException | DivideByZeroException e)`
2. A break should appear after for assignment operators
There are others listed in the linked document, but these first 2 are the most important.



