## OOP Example
This example will utilize the concepts of Session 3.

```
public interface HouseInterface {
    void hasBasement(boolean basement);
    void hasHardwoodFloors(boolean hardwoodFloors);
}

```

```
public abstract class House implements HouseInterface {
    private int externalDoors;
    private int internalDoors;
    private int numberOfWindows;
    private int numberOfGarageDoors;
    private String exteriorColor;
    private String roofType;
    private boolean hasHardwoodFloors;
    private boolean hasBasement;

    public House() {
        this(2, 10, 9, 2,"white","shingle");
    }

    public House(int externalDoors, int internalDoors, int numberOfWindows, int numberOfGarageDoors) {
        this(externalDoors,internalDoors,numberOfWindows,numberOfGarageDoors ,"white", "composite");
    }

    public House(int externalDoors, int internalDoors, int numberOfWindows,
    int numberOfGarageDoors ,String exteriorColor, String roofType) {
        this.externalDoors = externalDoors;
        this.internalDoors = internalDoors;
        this.numberOfWindows = numberOfWindows;
        this.numberOfGarageDoors = numberOfGarageDoors;
        this.exteriorColor = exteriorColor;
        this.roofType = roofType;
    }

    public void hasBasement(boolean basement) {
        hasBasement = basement;
    }

    public void hasHardwoodFloors(boolean hardwoodFloors) {
        hasHardwoodFloors = hardwoodFloors;
    }

    public int getExternalDoors() {
        return externalDoors;
    }

    public int getInternalDoors() {
        return internalDoors;
    }

    public int getNumberOfWindows() {
        return numberOfWindows;
    }

    public int getNumberOfGarageDoors() {
        return numberOfGarageDoors;
    }
    
    public String getExteriorColor() {
        return exteriorColor;
    }
    
    public String getRoofType() {
        return roofType;
    }
}

```

```
public class RanchHouse extends House{

    public RanchHouse() {
        this(3, 12,10);
    }

    public RanchHouse(int externalDoors, int internalDoors, int numberOfWindows) {
        super(externalDoors, internalDoors,numberOfWindows, 2,
                "Medium Gray","Composite");
    }

    @Override
    public void hasBasement(boolean basement ) {
        super.hasBasement(basement);
    }

    @Override
    public void hasHardwoodFloors(boolean floor) {
        super.hasHardwoodFloors(floor);
    }
}

```

```
public class TwoStoryHouse extends House{

    public TwoStoryHouse() {
        this(3, 18,20);
    }

    public TwoStoryHouse(int externalDoors, int internalDoors, int numberOfWindows) {
        super(externalDoors, internalDoors,numberOfWindows, 3,
                "Tan","Cedar");
    }
    
    @Override
    public void hasHardwoodFloors(boolean floor) {
        super.hasHardwoodFloors(floor);
    }

}

```

