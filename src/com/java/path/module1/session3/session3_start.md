# Object Oriented Programming
Object-Oriented Programming (OOP) is a programming paradigm based on the concept of objects. There are four pillars of 
Object-Oriented Programming:
- Abstraction
- Encapsulation
- Inheritance
- Polymorphism

In this session we will cover these 4 pillars in Java along with the following topics:
- Access Modifiers
- Static

Study the information below. Take notes. Once complete, begin your [Session Assignment](session3_assignment.md).
For a full example of most of the concepts in this session, refer to the [House](session3_house.md) example.

---

### Access Modifiers
Access Modifiers in Java specify accessibility scope of data members, methods, classes & constructors. They determine
the structure of the program. The modifier if applicable is the first type written before any other. These modifiers
consist of the following:
- private
- default
- protected
- public

##### Private Modifiers
Methods or data members declared as private are accessible only within the class in which they were created. Any class
created in the any package name will not have access to those members. In addition, classes can not be declared private.
Constructors inside of classes can be declared private under certain conditions.

##### Default Modifiers
When no access modifier is specified for a class, method or data member, it is said to have a default access modifier.
The default modifier allows accessibility within the same package. This means any data member within the package name
can access a default data member.

##### Protected Modifiers
Methods or data members declared as protected are accessible within the same package name, or a different package name
if the class is a subclass. As a result, classes can not use the protected modifier.

##### Public Modifiers
The public modifier has the largest scope of all access modifiers. There is no restriction in scope for this modifier. 
That means they can be accessed from anywhere. Keep this in mind when using modifiers as you may want to go with more 
restricted access if you know that only a certain scope requires it.


### Encapsulation
This pier of OOP is the process of wrapping data and the code into a single unit. Class level members are declared 
private, and methods for setting or getting their data is created where applicable. You can further limit access to
those class level variables based on 3 styles:
- Read/Write - Creating both a setter and getter
- Write Only - Creating only a setter
- Read Only - Creating only a getter. (Using a constructor to set the values)

With these 3 varied styles you can control the flow of information. Depending on the class makeup, you could utilize
all three. These are typically used in conjunction with defined constructors rather than the default constructor. 
Going along with Encapsulation is the **this** keyword.

##### this keyword
The **this** keyword is a reference variable that refers to the current class object. When assigning your property
values to your reference variable. Utilizing the keyword **this** prevents ambiguity between the parameter of a method
or constructor with the property value. If a parameter value and reference value are he same, without using the keyword
**this** will result in the compiler thinking that both values are the parameter.

```
// Read Only Example
class House {
    private int windowSize;
    private String doorColor;
    
    public House(int windowSize, String doorColor) {
        this.windowSize = windowSize;
        this.doorColor = doorColor;
    } 
    
    public int getWindowSize() {
        return windowSize;
    }
    
    public String getDoorColor() {
        return doorColor;
    }
}
```

### Abstraction
A process of exposing all necessary details and hiding everything else is defined as Abstraction in Java OOP. This Data
Abstraction is defined as only exposing parts to the user that are necessary. This comes as properties, methods, and 
interfaces. Java Data Abstraction can be defined using **Abstract classes** and **Interfaces**. 

##### Abstract Classes
This kind of class is created using the **abstract** keyword. These classes are also classes that can not have objects 
directly created. This kind of class is the highest hierarchy of an inherited model. Abstract classes are made up of
the following:
- abstract methods
  - Methods without a body
- non-abstract methods
  - normal methods or concrete method that have a body
- final and static methods

Non-Abstract (normal) classes can not have a defined abstract data member. Because we cannot create object of an
abstract type, we can only use the objects of its subclasses. There is always a default constructor in an abstract
class, and it can also have constructors that take parameters.

**Abstract Methods** are methods that have not body. Much like interfaces, these methods must be implemented in a
subclass that is not declared abstract. Abstract methods are terminated with a semi-colon instead of the method body.
The methods also use the **abstract** keyword when defining. You also cannot implement an abstract method in your
subclass with a more restricted access modifier that its definition in the abstract class.

``` 
public abstract NameClass {
    // abstract method
    protected abstract void something();
    
    // concrete method
    protected void other() {
        // code
    }
}
```

##### Interfaces
An interface is similar to a class. It is a blueprint or template for a class, but has abstracted methods and static
constants. These abstracted methods have no defined body or access modifiers which makes the implicitly abstract and
public. Like classes, they represent an **IS-A** relationship of inheritance between two classes. Classes can implement
multiple interfaces separated by commas. If a class implements an interface, they must implement the methods that make
up that interface, unless the class is **abstract**.

```
public interface NameInterface {
    void someMethod();
}
```

### Inheritance
A mechanism in which one or more classes acquire all the properties and behaviors of another class. The super class is
the class that gets inherited from. Subclasses are classes that inherit from the super class. This idea is to create
classes that are built upon other classes. The subclass can reuse methods and properties of the super class while also 
defining their own methods and properties. Inheritance represents the **IS-A** Relationship. Subclasses use the
**extends** keyword to create the relationship between them and a super class.

There are different types of Inheritance in Java:
- Single
  - Only 1 subclass inheriting from a super class.
- Multi-Level
  - The first subclass becomes a super class to another subclass.
  - Both subclasses inherit from the primary super class
- Hierarchical
  - More than 1 subclass inheriting from a super class.

```
class SuperClass {
  // defined code
}

class SubClass extends SuperClass {
  // inherited code from the super class
  // defined code for subclass
}
```

### Polymorphism
Polymorphism in an OOP concept which allows us to perform a single action in different ways. Polymorphism is derived
from 2 greek words: **poly** and **morphs**. The word *poly* means **many** and *morphs* means **forms**. So 
Polymorphism means **many forms**.

There are two types of Polymorphism in Java:
- Compile time polymorphism
- Runtime Polymorphism

##### Compile Time Polymorphism
Also called Static Polymorphism, compile time polymorphism is resolved when the application is being compiled. This
means that the compiler will resolve differences in code between different methods of the same name. These differences
are referred to a **method overloading**.

Method Overloading is a concept in which a method or constructor is defined more than once in a given class. This can 
be achieved by:
- Number of parameters
- Data type of parameters
- Sequence of Data type parameters

A couple of things to keep in mind when doing method overloading. Changing the names of a parameter, while leaving the 
type the same, does not work. A method's signature is based on the parameter data types rather than the named variables.
The compiler will still see the same signature, and therefore produce an error. Changing the sequence of data type
parameters, while allowed, should be discouraged. You should instead refactor the existing method or constructor. This
is because any other developer who will come back to this code, won't know which method or constructor to use. It is
best to either add or remove a parameter, or refactor the existing method or constructor to work with both situations.

You can also utilize the **this** keyword to pass information from one constructor to another. By doing this technique
you can provide default data information to the constructors with fewer parameters.

```
class SomeClass {
  public SomeClass() {
    this("something")
    // code goes here
  }
  public SomeClass(String val) {
    // code goes here
  }
}
```

##### Runtime Polymorphism
Also called Dynamic Polymorphism, this type resolves dynamically at runtime instead of at compile time. We achieve this
by way of **Method Overriding**. This type of overriding is done when a subclass implements a super class method and define
its own behavior. The subclass implements this by defining the super class method by name. Also, as a part of this, is
the use of the **@Override annotation**. This is usually done above the class. You will see the same annotation used
when implementing an Interface.

```
class Something {
  void someMethod() {
    // super class code
  }
}

class Other extends Something {
  @Override
  void someMethod() {
    // subclass code
  }
}
```

##### Super Keyword
Super keyword in Java is a reference variable that is used to refer to parent class objects. Super is an implicit 
keyword created by the JVM and supplies all java programs an important role in three places.
- At variable level
  - Use this level when a subclass and super class have the same data members.
- At method level
  - Use this level when you want to include the super class method code with the subclass override code.
- At constructor level
  - Use this level when you want to pass the subclass data to the parent, so the parent can set all the properties

Just like using the **this** keyword, the **super** can be used with subclass constructors to pass data to the parent.
A full example of how these are used can be seen in the [House](session3_house.md) example.

### Static
The static keyword in java is used for memory management.  The static keyword belongs to the class rather than instance
of the class. This basically means you don’t need to create an instance of an object to use it, just call it directly.

We can apply the keyword to:
- variables
- methods
- blocks
- nested classes

##### Static Variables
A static variable can be used to refer the common property of all objects. These variables get memory defined only once
in the class at the time of class loading.

They are initialized:
- When the class is loaded
- Before any object of that class can be created
- Before any static method of the class runs

```
class Something {
  static double pi = 3.14;
}
```

##### Constants
Like a static variable a constant utilizes the **static** keyword. It also utilizes the **final** keyword. Both of
these keywords have to be used together to create a constant. This type of variable cannot be changed once it is
defined. It also has to be initialized with a value. Because it uses the static keyword, it also is defined once and
does not have instances.

```
class Something {
  static final int MY_INT = 42;
}
```

##### Static Methods
A static method means *behavior not dependent on an instance variable, so no instance/object is required, just
the class.* A static method should be called using the class name rather than an object reference variable. Since a
static method is not associated with an instance of a class, instance variables can not be accessed. The same goes for
instance methods. You can not call an instance method inside a static method that is defined in the same class. IIf you
have a class with only static methods, and you do not want the class to be instantiated, you can mark the 
constructor private. Static methods can access static variables.

```
class Something {
  static void someMethod() {
    // code goes here
  }
}
```

##### Static Blocks
Static block is mostly used for changing the default values of static variables. This block gets executed when the 
class is loaded in the memory. A class can have multiple Static blocks, which will execute in the same sequence in 
which they have been written into the program. It is executed before main method at the time of class loading.

```
class Program {
  static in num;
  static String word;

  static {
      num = 97;
      word = “sample”
  }
  static {  num = 37;   }
}
```
