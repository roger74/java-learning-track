# Assignments

Complete the following code tasks to reinforce the session 3 content. Create at least 1 class file
to represent your session unless otherwise instructed. For this session you will create a file
called module1_3.java. This file will be used for some tasks. You will also create other java files
as defined in their tasks.

### Task 1
You're creating an application about comic books. Utilizing OOP, create a series of classes 
about DC, Marvel and Image comics. It should include all 4 pillars of OOP. Create all the classes
and files for this task that you need. You will not use the module1_3.java file for this task.

### Task 2
Using the module1_3.java class file, create four static class variables. Create and example of two
static blocks that each change two static variables. Include a main method in this file and print
out each of the four static variables. Ensure that their values changed as a result of the static
blocks.

### Task 3
Create a class file called task3.java. Create 3 constant numeric class variables in task3.java.
Write a method in the session3.java file that uses the constant values from take3.java. Test 
your method in the main method.